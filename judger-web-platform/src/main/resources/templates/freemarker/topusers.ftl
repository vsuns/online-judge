<!DOCTYPE html SYSTEM "http://www.thymeleaf.org/dtd/xhtml1-strict-thymeleaf-4.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">
<body>
<div class="sidebox roundbox" th:fragment="web-sidebar-top10" >
    <div class="top-link">
        <div class="title-sidebox">
            <li style="position:relative;list-style-type:none; ">
                <b style="width:24px;height:24px;background:url(/img/top_24.png) no-repeat;display:block;position:absolute;left:0px;top:3px;"></b>
                <span class="title" th:text="Top10"></span>
            </li>
        </div>
    </div>
    <div class="top10">
        <table class="standings">
            <tbody>
            <#list topusers as e>
                <#assign x=e_index>
                <tr>
                    <td class="sidebar-rank left-item <#if (x%2=1)>dark</#if> <#if e_has_next><#else>bottom</#if>">
                        <#if (x==0)><img class="rank-icon" src="/img/gold.png">
                        <#else>
                            <#if (x==1)><img class="rank-icon" src="/img/silver.png">
                            <#else>
                                <#if (x==2)><img class="rank-icon" src="/img/copper.png">
                                <#else>
                                    ${e_index+1}
                                </#if>
                            </#if>
                        </#if>
                    </td>
                    <td class="user <#if (x%2=1)>dark</#if> <#if e_has_next><#else>bottom</#if>">
                        <b><span  class="coder" ><a href="/profile/${e.username}" title="${e.username}" class="user-rate-${e.rate} user-tip" user="${e.username}">
                        <#if e.avatar?exists>
                            <img class="mini-avatar" src="${e.avatar}" />
                        <#else></#if>
                        <#if e.nickname?exists>
                            <#if e.nickname?length=0>${e.username}
                            <#else>${e.nickname}</#if>
                        <#else>${e.username}</#if>
                        </a></span></b>
                    </td>
                    <td class=" <#if (x%2=1)>dark</#if> <#if e_has_next><#else>bottom</#if>">
                        ${e.rating?c}
                    </td>
                </tr>
            </#list>
            </tbody>
        </table>
    </div>
    <div class="bottom-link">
        <table style="width: 100%;">
            <tbody>
            <tr>
                <td style="text-align: left;">
                </td>
                <td style="text-align: right;">
                    <a href="/problemset/standings" >&rarr;</a>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>

