<!DOCTYPE html SYSTEM "http://www.thymeleaf.org/dtd/xhtml1-strict-thymeleaf-4.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">
<body>
<div class="sidebox roundbox" th:fragment="web-sidebar-tags" >
    <div class="top-link">
        <div class="title-sidebox">
            <li style="position:relative;list-style-type:none; ">
                <b style="width:24px;height:24px;background:url(/img/tags_24.png) no-repeat;display:block;position:absolute;left:0px;top:3px;"></b>
                <span class="title" >Hot Tags</span>
            </li>
        </div>
    </div>
    <div class="tags tags-sidebar" style="">
        <#list toptags as e>
            <a href="/tags" class="tag" tag="${e.name}">
                <span class="tag-name">${e.name}</span>
                <span class="tag-count">${e.count}</span>
            </a>
        </#list>
    </div>
    <div class="bottom-link">
        <table style="width: 100%;">
            <tbody>
            <tr>
                <td style="text-align: left;">
                </td>
                <td style="text-align: right;">
                    <a href="/tags" >&rarr;</a>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>


