<!DOCTYPE html SYSTEM "http://www.thymeleaf.org/dtd/xhtml1-strict-thymeleaf-4.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">
<body>
<div class="sidebox roundbox" th:fragment="web-sidebar-topics" >
<div class="top-link">
    <div class="title-sidebox">
        <li style="position:relative;list-style-type:none; ">
            <b style="width:24px;height:24px;background:url(/img/topic_24.png) no-repeat;display:block;position:absolute;left:0px;top:3px;"></b>
            <span class="title" th:text="Topics"></span>
        </li>
    </div>
</div>
<div class="latest_news" style="">
    <ul>
        <#list topics as e>
            <li class="latest_news_item" url="/topic/<#if e.parent_id != 0 >${e.root_id?c}#rpl_${e.message_id?c}"<#else>${e.message_id?c}"</#if>>
                <div class="title">
                    <a title="${e.user.username}" href="/profile/${e.user.username}" class="rated-user user-rate-${e.user.rate} user-tip" user="${e.user.username}">
                    <img class='mini-avatar' src='${e.user.avatar}'>
                    </a>
                    ${e.title}
                </div>
                 <div class="content">
                     ${e.content_abstract}
                </div>
            </li>
        </#list>
    </ul>
</div>
<div class="bottom-link">
    <table style="width: 100%">
        <tbody>
        <tr>
            <td style="text-align: left;">
            </td>
            <td style="text-align: right;">
                <a href="/topics" >&rarr;</a>
            </td>
        </tr>
        </tbody>
    </table>
</div>
</div>
</body>
</html>
