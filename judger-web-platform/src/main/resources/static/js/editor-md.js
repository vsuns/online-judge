
$(document).ready(function () {
    var editor_md_views = $("div.markdown-view")
    for(var i=0; i < editor_md_views.length; i++) {
        var editor_md_view = editormd.markdownToHTML(editor_md_views[i].id, {
            tocm : true,
        });
    }
});
function EditorMarkDown(){
    this.createEditorMDEx = function(id){
        var editor = editormd(id, {
            height : "600px",
            path   : "/js/editormd/lib/",
            toolbarIcons : function() {
                return ["bold", 'italic', "del", "hr", "h1", "h2", "h3", "list-ul", "list-ol", "table", "link",
                    "code", "code-block", "emoji", "image", "preview", "watch", "info", "||", "fullscreen"]
            },
            placeholder: "",
            watch: false,
            emoji: true,
            lineNumbers: false,
            flowChart: true,
            imageUpload: true,
            imageFormats: ["jpg", "jpeg", "gif", "png", "bmp", "webp"],
            imageUploadURL : "/api/upload/image",
            onload : function() {
                EditorMarkDown.editorPasteDragImg(this)
            }
        });
    }
    this.createEditorMDWithHeight = function(id, height){
        var editor = editormd(id, {
            height : height,
            path   : "/js/editormd/lib/",
            toolbarIcons : function() {
                return ["bold", 'italic', "list-ul", "list-ol", "table", "link",
                    "code", "code-block", "image", "watch", "||", "fullscreen"]
            },
            placeholder: "",
            watch: false,
            emoji: true,
            lineNumbers: false,
            flowChart: true,
            imageUpload: true,
            imageFormats: ["jpg", "jpeg", "gif", "png", "bmp", "webp"],
            imageUploadURL : "/api/upload/image",
            onload : function() {
                EditorMarkDown.editorPasteDragImg(this);
            }
        });
    }

    this.createEditorMD = function(id){
        EditorMarkDown.createEditorMDWithHeight(id, "250px");
    }

    this.editorPasteDragImg = function(editor){
        var doc = document.getElementById(editor.id)
        doc.addEventListener('paste', function (event) {
            var items = event.clipboardData.items;
            if (items && (items[0].type.indexOf('image') > -1)) {
                var file = items[0].getAsFile();
                EditorMarkDown.uploadImg(file, editor)
            }
        });
        var dashboard = document.getElementById(editor.id)
        dashboard.addEventListener("dragover", function (e) {
            e.preventDefault()
            e.stopPropagation()
        })
        dashboard.addEventListener("dragenter", function (e) {
            e.preventDefault()
            e.stopPropagation()
        })
        dashboard.addEventListener("drop", function (e) {
            e.preventDefault()
            e.stopPropagation()
            var files = this.files || e.dataTransfer.files;
            EditorMarkDown.uploadImg(files[0], editor);
        })
    }

    this.uploadImg = function(file, editor){
        var newImg = new Date().getTime() + '.png';
        var formData = new FormData();
        formData.append('file', file, newImg)
        $.ajax({
            type: "POST",
            url: "/api/upload/image",
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {
                console.log(data.data);
                var src = data.data;
                editor.insertValue("![" + newImg + "](" + src + ")");
            },
            error: function (data) {
                console.log("Error: "+ data.data);
            }
        })
    }
}
EditorMarkDown = new EditorMarkDown();
;

$(document).ready(function(){
    var editors = $("div.editor-md")
    for(var i=0; i < editors.length; i++) {
        EditorMarkDown.createEditorMD(editors[i].id)
    }

    var editorsMax = $("div.editor-md-max")
    for(var i=0; i < editorsMax.length; i++) {
        EditorMarkDown.createEditorMDEx(editorsMax[i].id)
    }
});
