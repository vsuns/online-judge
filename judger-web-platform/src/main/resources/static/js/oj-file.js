var editorOjFile = null;
var OjFileContent = ""
var OjFileShowStop = true;
var g_autoScrollOjFile = true;
var g_fromLineNumOjFile = 0;
var g_readLineNumOjFile = 1000;
var g_OjFile = ""
var g_showType = ""

$(document).ready(function () {
    function showDir(current) {

        var formData = {"type": g_showType, "current": current}
        $.ajax({
            type : "post",
            url : "/api/admin/dir",
            data : JSON.stringify(formData),
            contentType:"application/json",
            dataType : "json",
            success : function(data) {
                if (data.code != 200) {
                } else {
                    $(".dir-title").html(" <b> " + g_showType + " Directory of " + data.data.current + ":</b>");
                    var buf = "<input type='hidden' id='current-dir' value='"+ data.data.current + "'/>";
                    //buf += " <b> " + g_showType + " Directory of " + data.data.current + ":</b><br>";
                    buf += "<table class='tb-dir'>";
                    buf += " <tr class='header'><th class='tb-dir-attr'>Attr</rh><th class='tb-dir-size'>File Size(Byte)</rh><th class='tb-dir-space'></th><th class='tb-dir-date'>Date</rh><th class='tb-dir-name'>File Name</rh><tr>";
                    buf += " <tr><td class='tb-dir-attr'>drw-</td><td class='tb-dir-size'>-</td><th class='tb-dir-space'></th><td class='tb-dir-date'>-</td><td class='tb-dir-name'><a class='chg-dir' dir='..'" + ">Parent directory</a></td><tr>";
                    $.each(data.data.files, function(i, event) {
                        var f = data.data.files[i]
                        if (f.isDirectory == true) {
                            buf += " <tr><td class='tb-dir-attr'>drw-</td><td class='tb-dir-size'>"+ "-" +"</td><th class='tb-dir-space'></th><td class='tb-dir-date'>" + f.lastModified + "</td><td class='tb-dir-name'><a class='chg-dir' dir='" + f.name + "'>" + f.name + "</a></td><tr>";
                        } else {
                            buf += " <tr><td class='tb-dir-attr'>-rw-</td><td class='tb-dir-size'>"+f.length+"</td><th class='tb-dir-space'></th><td class='tb-dir-date'>" + f.lastModified + "</td><td class='tb-dir-name'><a class='show-oj-file' filename='" + f.name + "'>" + f.name + "</a></td><tr>";
                        }
                    });
                    buf += "</table>"
                    $(".dir-show").html(buf);
                }
            }
        });
    }

    $(document).on("click",".chg-dir",function(){
        var current = document.getElementById('current-dir').value;
        showDir(current + $(this).attr("dir"));
    });

    var showDirDiv = function () {
        var scrollTop = window.pageYOffset||document.documentElement.scrollTop || document.body.scrollTop;
        var topPosInner = 60 + scrollTop;
        var leftPosIner = ($(document.body).width() - 1200)/2;
        document.getElementById('pop-inner-div-dir').style.top= topPosInner +'px';
        document.getElementById('pop-inner-div-dir').style.left= leftPosIner +'px';
        document.getElementById('pop-div-dir').style.top= scrollTop +'px';
        document.documentElement.style.overflowY = 'hidden';

        $(".pop-div-dir").fadeIn();
        $(".pop-inner-div-dir").fadeIn();

        showDir("");
    };

    $(".show-oj-dir").click(function(){
        g_showType = "OJ";
        showDirDiv();
    });

    $(".show-logs-dir").click(function(){
        g_showType = "LOGS";
        showDirDiv();
    });

    $(".pop-div-dir").click(function(){
        $(".pop-div-dir").fadeOut();
        $(".pop-inner-div-dir").fadeOut();
        document.documentElement.style.overflowY = 'scroll';
    });

    var createOjFileEditor = function () {
        require.config({paths:{'vs':'/js/monaco-editor/min/vs'}});
        require(['vs/editor/editor.main'], function() {
            editorOjFile = monaco.editor.create(document.getElementById("oj-file-show"), {
                value: [].join('\n'),
                readOnly: true,
                language: "cpp",
                fontSize: 12 +"px",
                minimap: {
                    enabled: false
                },
                wordWrap: "on",
                automaticLayout: true,
                scrollBeyondLastLine: false
            });
            if (editorOjFile) {
                editorOjFile.layout();
            }
        });
    }

    createOjFileEditor();

    function loadOjFile(filePath, fromLineNum, readLineNum) {
        if (OjFileShowStop) {
            return;
        }
        var formData = { "type":g_showType, "fileName": filePath, "content": "",
            "fromLineNum": fromLineNum, "readLineNum": readLineNum,
            "nextLineNum": 0}
        $.ajax({
            type : "post",
            url : "/api/admin/get-oj-file",
            data : JSON.stringify(formData),
            contentType:"application/json",
            dataType : "json",
            success : function(data) {
                if (data.code != 200) {
                } else {
                    OjFileContent = OjFileContent + data.data.content;
                    editorOjFile.setValue(OjFileContent);
                    editorOjFile.layout();
                    if (g_autoScrollOjFile) {
                        editorOjFile.revealLine(OjFileContent.split('\n').length);
                    }
                    if (g_fromLineNumOjFile == data.data.nextLineNum) {
                        g_fromLineNumOjFile = data.data.nextLineNum;
                        setTimeout(function () { loadOjFile(filePath, data.data.nextLineNum, g_readLineNumOjFile); }, 3000);
                    } else {
                        g_fromLineNumOjFile = data.data.nextLineNum;
                        setTimeout(function () { loadOjFile(filePath, data.data.nextLineNum, g_readLineNumOjFile); }, 1000);
                    }
                }
            }
        });
    }

    function showOjFile(ojFile) {
        if (g_OjFile != ojFile) {
            $(".oj-file-title").html(ojFile);
            OjFileContent = "";
            g_fromLineNumOjFile = 0;
            g_OjFile = ojFile;
            editorOjFile.setValue("");
            editorOjFile.layout();
        }

        var scrollTop = window.pageYOffset||document.documentElement.scrollTop || document.body.scrollTop;
        var topPosInner = 60 + scrollTop;
        var leftPosIner = ($(document.body).width() - 1200)/2;
        document.getElementById('pop-inner-div-show-oj-file').style.top= topPosInner +'px';
        document.getElementById('pop-inner-div-show-oj-file').style.left= leftPosIner +'px';
        document.getElementById('pop-div-show-oj-file').style.top= scrollTop +'px';
        document.documentElement.style.overflowY = 'hidden';

        $(".pop-div-show-oj-file").fadeIn();
        $(".pop-inner-div-show-oj-file").fadeIn();

        OjFileShowStop = false;
        setTimeout(function () { loadOjFile(ojFile, g_fromLineNumOjFile, g_readLineNumOjFile); }, 1000);
    }
    $(".pop-div-show-oj-file").click(function(){
        OjFileShowStop = true;
        $(".pop-div-show-oj-file").fadeOut();
        $(".pop-inner-div-show-oj-file").fadeOut();
        document.documentElement.style.overflowY = 'scroll';
    });
    $(document).on("click",".show-oj-file",function(){
        var current = document.getElementById('current-dir').value;
        showOjFile(current + $(this).attr("filename"));
    });
    $(".dir-show-menu").click(function() {
        if (g_autoScrollOjFile) {
            g_autoScrollOjFile = false;
            $(".dir-show-menu").html("Auto to bottom");
        } else {
            g_autoScrollOjFile = true;
            $(".dir-show-menu").html("Cancel auto to bottom");
        }
    });
});