function ReplyInline(messageId, rootId) {
    $('.inline_reply').empty();
    var div_id = '#inline_reply_of_' + messageId + '_' + rootId;
    $('#inline_parentId').val(messageId);
    $('#inline_rootId').val(rootId);
    $(div_id).html($('#inline_reply_editor').html());
    $('#txt_focus').focus();
    $('#btn_close_inline_reply').click(function () {
        $(div_id).empty();
    });

    EditorMarkDown.createEditorMD("editor-topic-inline-reply-comment")

    var commentInlineReplyFormSubmitListener = function () {
        var id = $(this).attr("id");
        var moduleId = $(this).find("input[name=moduleId]").val();
        var parentId = $(this).find("input[name=parentId]").val();
        var rootId = $(this).find("input[name=rootId]").val();
        var content = $(this).find("textarea[name=content]").val();

        postNewCommentReply = function () {
            $.post("/api/topic/post", { type: "new", parentId: parentId, rootId: rootId, content: content },
                function (data) {
                    if (data.code != 200) {
                        $.messager.show("Error", data.msg, 5000);
                        $('#inline-reply-content').focus();
                        return;
                    }

                    $('input[type=submit]', this).attr('disabled', 'disabled');
                    location.reload();
                },
                "json"
            );
        };
        postNewCommentReply();
        return false;
    }

    var init = function () {
        var e = $(".inline-reply-edit-box");
        $(".inline-reply-edit-box").find("form").submit(commentInlineReplyFormSubmitListener);
    }
    init();
}

function DeleteComment(messageId) {
    deleteComment = function () {
        $.post("/api/topic/post", { type: "delete", messageId: messageId},
            function (data) {
                if (data.code != 200) {
                    //alert(data.msg);
                    $.messager.show("Error", data.msg, 5000);
                    return;
                }
                location.reload();
            },
            "json"
        );
    };
    deleteComment();
    return false;
}

$(document).ready(function () {
    var commentReplyFormSubmitListener = function () {
        var id = $(this).attr("id");
        var type = $(this).find("input[name=type]").val();
        var messageId = $(this).find("input[name=messageId]").val();
        var problemId = $(this).find("input[name=problemId]").val();
        var moduleId = $(this).find("input[name=moduleId]").val();
        var parentId = $(this).find("input[name=parentId]").val();
        var rootId = $(this).find("input[name=rootId]").val();
        var content = $(this).find("textarea[name=content]").val();
        var title = $(this).find("input[name=title1]").val();
        var tags = $(this).find("input[name=tagList]").val();
        postNewCommentReply = function () {
            $.post("/api/topic/post",{ type: type, messageId:messageId, problemId:problemId,moduleId: moduleId, parentId: parentId, rootId: rootId, content: content, title: title, tags:tags },
                function (data) {
                    if (data.code != 200) {
                        $(".fielderror").html(data.msg);
                        $.messager.show("Error", data.msg, 5000);
                        $("#reply-content").focus();
                        return;
                    }

                    $('input[type=submit]', this).attr('disabled', 'disabled');
                    $('#btn_comment').attr('disabled', 'disabled');
                    window.location.href="/topic/"+data.data;
                },
                "json"
            );
        };
        postNewCommentReply();
        return false;
    }

    var init = function () {
        $(".reply-edit-box").find("form").submit(commentReplyFormSubmitListener);
        $(".message-edit-box").find("form").submit(commentReplyFormSubmitListener);
        $(document).on("click",".abstract",function() {
            var messageId = $(this).attr("messageId");
            window.location = "/topic/"+messageId;
            return false;
        });
    };
    init();

    $(".reply a").click(function () {
        window.location = "/enter";
        return false;
    });

    function ShowTagModifyArea(element) {
        var topicId = element.attr("topicId");
        var orderNum = element.text().trim();
        var appendHtml ="<input id='temp' class=''></input>";
        var orderElmt = element.children(".order-num");
        orderElmt.text("");
        orderElmt.append(appendHtml);
        $("input#temp").focus();
        $("input#temp").val(orderNum);
        $("input#temp").blur(function(){
            var new_orderNum = $(this).val();
            if (orderNum === new_orderNum) {
                orderElmt.html(new_orderNum);
                return;
            }
            $.post("/api/admin/topic/order",{ topicId: topicId, orderNum: new_orderNum},
                function (data) {
                    if (data.code !== 200) {
                        $.messager.show('Tip', data.msg, 5000);
                        $("input#temp").focus();
                        return;
                    }
                    orderElmt.html(new_orderNum);
                    $.messager.show('Tip', "Modified successfully", 5000);
                },
                "json"
            );
        });
    }

    $(document).on("click",".topic-order-num",function(){
        var element = $(this);
        $.getJSON("/api/role/check", {},function(json) {
            if (json.code === 200) {
                ShowTagModifyArea(element);
            }
        });
    });

    function ShowOrderNum() {
        $.getJSON("/api/role/check", {},function(json) {
            if (json.code === 200) {
                $(".topic-order-num").css("display", "inline");
            }
        });
    }
    ShowOrderNum();
});