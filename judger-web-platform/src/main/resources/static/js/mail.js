$(document).ready(function () {
    function getElementById(id) {
        return document.getElementById(id);
    }
    var oBox = getElementById("mail-box");
    var oLeft = getElementById("mail-box-left");
    var oRight = getElementById("mail-box-right");
    var oDragbar = getElementById("mail-box-drag-bar");
    var oHeaderNavbar = getElementById("header-navbar");
    var oBottombarLeft = getElementById("bottom-bar-left");
    var oBottombarRight = getElementById("bottom-bar-right");
    var oMailBoxNav = getElementById("mail-box-nav");
    var oMailTableBox = getElementById("mail-box-tables");
    var oMailContentBox = getElementById("mail-content-box");

    function refleshSize() {
        oLeft.style.height = window.innerHeight - oHeaderNavbar.offsetHeight - oMailBoxNav.offsetHeight + "px";
        oRight.style.height = window.innerHeight - oHeaderNavbar.offsetHeight + "px";
        oRight.style.left = oDragbar.offsetLeft + oDragbar.offsetWidth + "px";
        oRight.style.width = window.innerWidth - oDragbar.offsetLeft - oDragbar.offsetWidth + "px";
        oMailTableBox.style.height = window.innerHeight - oHeaderNavbar.offsetHeight -
            oBottombarLeft.offsetHeight  - oMailBoxNav.offsetHeight + "px";
        oBottombarLeft.style.width = oDragbar.offsetLeft + "px";
        oBottombarRight.style.width = window.innerWidth - oDragbar.offsetLeft - oDragbar.offsetWidth + "px";
        oMailContentBox.style.height = window.innerHeight - oHeaderNavbar.offsetHeight -
            oBottombarRight.offsetHeight + "px";
    }

    window.onload = function () {
        refleshSize();
        oDragbar.onmousedown = function (e) {
            var disX = (e || event).clientX;
            oDragbar.left = oDragbar.offsetLeft;

            document.onmousemove = function (e) {
                var iT = oDragbar.left + ((e || event).clientX - disX);
                var e = e || window.event, tarnameb = e.target || e.srcElement;
                var maxT = oBox.clientWidth - oDragbar.clientWidth;
                oDragbar.style.margin = 0;
                iT < 0 && (iT = 0);
                iT > maxT && (iT = maxT);

                if (iT <= 400 || oBox.clientWidth - iT - 1 <= 350) {
                    return false
                }

                oDragbar.style.left = oLeft.style.width = iT + "px";
                oRight.style.width = oBox.clientWidth - iT - oDragbar.offsetWidth + "px";
                oBottombarLeft.style.width = iT + "px";
                oBottombarRight.style.width = oBox.clientWidth - iT - oDragbar.offsetWidth + "px";

                return false
            };

            document.onmouseup = function () {
                document.onmousemove = null;
                document.onmouseup = null;
                oDragbar.releaseCapture && oDragbar.releaseCapture()
            };
            oDragbar.setCapture && oDragbar.setCapture();
            return false
        };
    };
    window.onresize = function () {
        refleshSize();
    }

    var showMailById = function (id) {
        $.getJSON("/api/mail/query",{id: id},function(json){
            if (json.code != 200) {
                $(".mail-content-data").html("<b>"+json["error"]+"</b>");
                return false;
            }
            $(".mail-content-data").html("");
            $(".mail-content-info").html("");

            var mail = json.data;
            var sendUser = json.data.sendUser
            var recvUser = json.data.recvUser
            var buf = "<div class=\'UserOutline\'>"
            buf += "<div class='u'>";
            buf += "<a href='/profile/"+ sendUser.username + "'>";
            buf += "<img style='width:80px;height:80px;' src='/img/photo.png' alt='" + sendUser.username + "'>";
            buf += "</a>";
            buf += "</div>"
            buf+="<a href='/mails/new/"+ sendUser.username + "' class='user-tip rated-user user-rate-"+ sendUser.rate+"'" + "user='" + sendUser.username + "'" +">" +sendUser.username+"</a>" +
                "<span class='city'>";
            buf+="</span>";
            buf+="<div id='UserMotto'>";
            buf+= mail.title;
            buf+= "</div><div style='clear:both'></div>";
            buf+= "<div>收件人: ";
            buf+= "<a href='/mails/new/"+ recvUser.username + "' class='user-tip rated-user user-rate-"+ recvUser.rate+"'" + "user='" + recvUser.username + "'" +">" + recvUser.username+"</a>";
            buf+= "</div>";
            $(".mail-content-info").html(buf);
            $(".mail-content-data").html(mail.content);
        });
    }
    var mailOpenListener = function () {
        var id = $(this).attr("id");
        showMailById(id);
    }
    $("tr[class='mail-item']").click(mailOpenListener);

    var showFirtMail = function () {
        var id = $("tr:nth-child(2)").attr("id");
        if (id != null) {
            showMailById(id);
        } else {
            $(".mail-content-box").html("");
        }
    }
    showFirtMail();

    var mailReplyFormSubmitListener = function() {
        var id = $(this).attr("id");
        var sendto = $(this).find("input[name=sendto]").val();
        var reply =	$(this).find("input[name=reply]").val();
        var title = $(this).find("input[name=title1]").val();
        var content = $(this).find("textarea[name=content]").val();
        postNewMailReply = function() {
            $.post("api/mail/postmail",{sendto:sendto, reply:reply, title: title, content: content},function(data) {
                if (data.code != 200) {
                    $(".fielderror").html(data.error);
                    return;
                }
                $('input[type=submit]', this).attr('disabled', 'disabled');
                window.location.href="/mails/send";
            }, "json");
        };
        postNewMailReply();
        return false;
    }

    var init = function() {
        var e = $(".mail-edit-box");
        $(".mail-edit-box").find("form").submit(mailReplyFormSubmitListener);
    }
    init();
});