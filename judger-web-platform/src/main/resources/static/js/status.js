var popTimes = 0;

$(document).ready(function(){
	$(document).on("click",".view-source",function(){
		$(".pop-inner-div p.pop-div-note").html("<img src='/img/loader.gif'>");
		$(".pop-inner-div p.pop-div-content").html(""); 
		$(".pop-inner-div .pop-div-source").html("");			
		 var solutionId = $(this).attr("solutionId");	 
		 var scrollTop = window.pageYOffset||document.documentElement.scrollTop || document.body.scrollTop;
		 var topPosInner = 60 + scrollTop;
		 document.getElementById('pop-inner-div').style.top= topPosInner +'px';
		 document.getElementById('pop-div').style.top= scrollTop +'px';

		 document.documentElement.style.overflowY = 'hidden';
		 $(".pop-div").fadeIn(); 
		 $(".pop-inner-div").fadeIn();
		popTimes++;

		 var opt="";
		 $.getJSON("/api/solution/query",{solutionId: solutionId},function(json){
			 	if (json.code != 200) {
	            	$(".pop-inner-div p.pop-div-note").html("<b>"+json.msg+"</b>");
	            	return false;
	            }
	         	$(".pop-inner-div p.pop-div-note").html("");
	         	var solution = json.data;
	         	opt+="<a href='/profile/"+solution.username+"'"
	         			+ " class='rated-user user-rate-" + solution.user.rate + " user-tip'"
	         			+ " user='" + solution.username +"'>"
	         			+ solution.username+ "</a> , ";
			 	opt+= " <span title='" + solution.submit_date + "'>" + solution.friendlySubmitDate + "</span> , ";
				if(solution.contest_id>0){
					opt+=" <a style='color:#00C;' title='"+solution.contest_id+"' href='/contest/"+solution.contest_id+"/problem/"+ solution.contestProblem.num +"'>" + solution.contestProblem.num + ". " + solution.contestProblem.title + "</a>";
				}else{
					opt+=" <a style='color:#00C;' href='/problemset/problem/"+ solution.problem_id +"'>" + solution.problem_id + ". " + solution.problem.title + "</a>";
				}										
	         	
	         	opt+=" , <span class='";
	         	if(solution.verdict==5){
	         		opt+="verdict_ac";
	         	}else if(solution.verdict==3){
	         		opt+="verdict_ce";
	         	}else if(solution.verdict==10){
	         		opt+="verdict_pe";
	         	}else if(solution.verdict < 5){
	         		opt+="verdict_prev";
	         	}else{
	         		opt+="verdict_other";
	         	}
	         	opt+="'>";
	         	
	         	if(solution.verdict==3){
	         		opt+="<a style='color:#00C;' solutionId='"+ solution.solution_id +"' class='viewCompileInfo' href='/view-compileinfo/"+ solution.solution_id + "'>" + solution.status_description + "</a>";
	         	}else {
	         		opt+=solution.status_description ;
					if(solution.verdict==5){
					}
					else if(solution.verdict==4){
						opt += " on test "+solution.testcase;
					}else if(solution.verdict>5){
						opt += " on test "+solution.testcase;
					}						
	         	}		         	 		
	         	opt+="</span>";
	         	opt+="<hr>";
	         	opt+="<pre class='prettyprint'>";
	            opt+=solution.solutionSource.source;
	            opt+="</pre>";
				$(".pop-inner-div .pop-div-source").html(opt);
				
				opt="<h5>Judge Log:</h5>" ;
				opt+="<pre class='prettyprint '>";
				opt+=solution.judgeLog;
	            opt+="</pre>";
				
				$(".pop-inner-div .pop-div-judgelog").html(opt);
	   			prettyPrint();
	   		 }); 
	   		 return false;
	    });

	$(document).on("click",".viewCompileInfo",function(){
		$(".pop-inner-div p.pop-div-note").html("<img src='/img/loader.gif'>");
		$(".pop-inner-div p.pop-div-content").html(""); 
		$(".pop-inner-div .pop-div-source").html("");			
		var solutionId = $(this).attr("solutionId");
		var scrollTop = window.pageYOffset||document.documentElement.scrollTop || document.body.scrollTop;
		var topPosInner = 60 + scrollTop;
		document.getElementById('pop-inner-div').style.top= topPosInner +'px';
		document.getElementById('pop-div').style.top= scrollTop +'px';

		popTimes++;
		$(".pop-div").fadeIn(); 
		$(".pop-inner-div").fadeIn();
		document.documentElement.style.overflowY = 'hidden';
		 var opt="";
		 $.getJSON("viewcompileinfo",{solutionId: solutionId},function(json){ 	
            if (json.success != true) {
            	$(".pop-inner-div p.pop-div-note").html("<b>"+json["error"]+"</b>");
            	return false;
            }
         	$(".pop-inner-div p.pop-div-note").html("");
         	opt+="<a title='Open at new window'style='color:#00C;' href='/view-compileinfo/"+ json.solutionId + "'  target='_blank'>#"+ json.solutionId +"</a>";
            opt+="<pre class='code'>";
            opt+=json.errorInfo;
           	opt+="</pre><br>";
			$(".pop-inner-div .pop-div-source").html(opt);
   		 }); 
   		 return false;
	});

	$(".pop-div").click(function(){
		$(".pop-inner-div p.pop-div-content").html("");
		$(".pop-inner-div .pop-div-source").html("");
		$(".pop-inner-div .pop-div-judgelog").html("");
		$(".pop-div").fadeOut();
		$(".pop-inner-div").fadeOut();

		if (popTimes > 0) {
			popTimes--;
		}
		if (popTimes == 0){
			document.documentElement.style.overflowY = 'scroll';
		}
	});

	$(".view-submit-log").bind("dblclick", function() {
		var username = $(this).parent().attr("username");
		var problemId = $(this).attr("problemId");
		var contestId = $(this).attr("contestId");
		var scrollTop = window.pageYOffset||document.documentElement.scrollTop || document.body.scrollTop;
		var topPosInner = 60 + scrollTop;
		document.getElementById('pop-standing-inner-div').style.top= topPosInner +'px';
		document.getElementById('pop-standing-div').style.top= scrollTop +'px';

		document.documentElement.style.overflowY = 'hidden';
		$(".pop-standing-div").fadeIn();
		$(".pop-standing-inner-div").fadeIn();
		popTimes++;

		$(".pop-standing-inner-div p.pop-standing-div-note").html("<img src='img/loader.gif'>");
		$(".pop-standing-inner-div .pop-standing-div-source").html("");
		$.getJSON("/api/submission/query",{pageSize:100, contestId: contestId,problemId:problemId,contestOnly:1,username:username},function(json){
			if (json.code != 200) {
				$(".pop-standing-inner-div p.pop-standing-div-note").html("<b>"+json["error"]+"</b>");
				return false;
			}
			$(".pop-standing-inner-div p.pop-standing-div-note").html("");
			var opt="<pre class='submissions'>";

			$.each(json.data, function(i, event) {
				opt+="&nbsp;"+json.data[i].timeSinceContestStart+" &nbsp;&nbsp;";
				if(problemId!=""){
					opt+="<a href='/contest/"+contestId+"/problem/"+json.data[i].problemNum+"' target='_blank'>"+json.data[i].problemNum+"</a>&nbsp;&nbsp; ";
				}
				opt+="<span style=\"color:";
				if(json.data[i].status_description=="Accepted"){
					opt+="green;font-weight:bold;\">";
				}else if(json.data[i].status_description=="Compilation Error"){
					opt+="#000;\">";
				} else{
					opt+="#00A;\">";
				}
				opt+=json.data[i].status_description+"&nbsp;&nbsp;</span>  &rarr; ";
				opt+="<a solutionId=\""+json.data[i].solution_id+"\" class=\"view-source\" href=\"/view-source/"+json.data[i].solution_id+"\" target=\"_blank\">#" + json.data[i].solution_id+"</a><br/>";
			});
			opt+="</pre><br>";
			$(".pop-standing-inner-div .pop-standing-div-source").html(opt);
		});
	});

	$(".pop-standing-div").click(function(){
		$(".pop-standing-inner-div p.pop-standing-div-content").html("");
		$(".pop-standing-inner-div .pop-standing-div-source").html("");
		$(".pop-standing-div").fadeOut();
		$(".pop-standing-inner-div").fadeOut();

		if (popTimes > 0) {
			popTimes--;
		}
		if (popTimes == 0){
			document.documentElement.style.overflowY = 'scroll';
		}
	});

	$("a.re-judge").click(function() {
		if(confirm('WARNING! Do you really want to rejudge this submission?')){
			var solutionId = $(this).attr("solutionId");
			var formData = { "solutionId": solutionId, "problemId": 0};
			$.ajax({
				type : "post",
				url : "/api/admin/rejudge",
				data : JSON.stringify(formData),
				contentType:"application/json",
				dataType : "json",
				success : function(data) {
					if (data.code !== 200) {
						$.messager.show('Tips',
							"rejudge submission "+solutionId+" failed, errorMsg: " + data.msg,
							5000);
					} else {
						window.location.reload();
					}
				}
			});
		} else {
			return false;
		}
	});

	$.getJSON("/api/role/check", {},function(json) {
		if (json.code === 200) {
			$(".re-judge").css("display", "inline");
		}
	});
});