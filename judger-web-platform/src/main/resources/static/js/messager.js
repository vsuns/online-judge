(function (jQuery) {
    var window;
    var obj = new Object();
    obj.title = "";
    obj.time = 4000;
    obj.anims = { 'type': 'slide', 'speed': 600 };
    obj.inits = function (title, text, time) {
        window = $('<div class="messager"><div class="messager-header"><div class="messager-close">×</div><div class="meaaager-title">'+title+'</div></div> <div class="messager-bottom"><div class="messager_content">'+text+'</div></div></div>')
            .appendTo('body')
            .hide();
        window.find('div.messager-close').click(function () {
            this.parentElement.parentElement.remove();
        });
    };

    obj.show = function (title, text, time) {
        if (title == 0 || !title) title = obj.title;
        obj.inits(title, text, time);
        if (time >= 0) obj.time = time;
        switch (this.anims.type) {
            case 'slide': window.slideDown(obj.anims.speed).delay(time).fadeOut();; break;
            case 'fade': window.fadeIn(obj.anims.speed); break;
            case 'show': window.show(obj.anims.speed); break;
            default: window.slideDown(obj.anims.speed); break;
        }
    };

    obj.anim = function (type, speed) {
        if (type != 0 && type) obj.anims.type = type;
        if (speed != 0 && speed) {
            switch (speed) {
                case 'slow':; break;
                case 'fast': obj.anims.speed = 200; break;
                case 'normal': obj.anims.speed = 400; break;
                default:
                    obj.anims.speed = speed;
            }
        }
    };
    jQuery.messager = obj;
    return jQuery;
})(jQuery);