$(document).ready(function() {
    $.getJSON("/api/role/check", {},function(json) {
        if (json.code === 200) {
            $(".verify-problem").css("display", "inline");
            $(".new-problem").css("display", "inline");
            $(".edit-problem").css("display", "inline");
            $(".problem-data-link").css("display", "inline");
            $(".start-contest-update").css("display", "inline");
        }
    });
});