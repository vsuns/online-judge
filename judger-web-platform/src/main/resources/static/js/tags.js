$(document).ready(function(){
    var cache = {};
    var tagsMenuShow = false;
    function tagsSelectMenuShow(enable) {
        if (enable) {
            tagsMenuShow = true;
        } else {
            tagsMenuShow = false;
        }
    }
    function showSelectTags(tags) {
        $(".tags-autocomplete-select-items").html("");
        $(".tags-autocomplete-select-menu").fadeIn();
        var opt = "";
        $.each(tags, function(i, event) {
            var tagList = $('#submit-tags').val().split(",");
            if (tagList.indexOf(tags[i]) == -1) {
                opt += "<div class=\"tags-autocomplete-select-item\" data-value=\""+ tags[i] +"\">" + tags[i] +"</div>";
            }
        });
        $(".tags-autocomplete-select-items").html(opt);
    }
    function searchTags(word) {
        if ( word in cache ) {
            showSelectTags(cache[ word ]);
        } else {
            $.getJSON("/api/tags/search",{q: word},function(json) {
                if (json.code == 200) {
                    cache[ word ] = json.data;
                    showSelectTags(json.data);
                }
            });
        }
    }
    function findTagFromTagList(tag){
        var find = false;
        $("a.label-item").each(function(){
            var str = $(this).text();
            if (tag === str) {
                find = true;
                return true;
            }
        });
        return find;
    }
    function getTagNum(tag){
        var n = 0;
        $("a.label-item").each(function(){
            n = n + 1;
        });
        return n;
    }
    function insertShowTags(tags) {
        $.each(tags, function(i, event) {
            if (tags[i].length != 0) {
                if (findTagFromTagList(tags[i]) == true) {
                    $.messager.show('提示', '标签 ' + tags[i] + '已存在。', 5000);
                    return true;
                }
                if (getTagNum() >= 5) {
                    $.messager.show('提示', '最多添加5个标签。', 5000);
                    return true;
                }
                var opt = "<a class='label-item'>" + tags[i] + "<i class=\"icon-delete\"></i></a>";
                $(".add-tags-items").before(opt);
            }
        });
    }

    function updateSubmitTags() {
        var tagsList = "";
        $("a.label-item").each(function(){
            if (tagsList.length == 0) {
                tagsList = $(this).text();
            } else {
                tagsList += "," + $(this).text();
            }
        });
        $('#submit-tags').val(tagsList);
    }
    function insertTags(insertTag) {
        var tags = insertTag.split(",");
        insertShowTags(tags);

        /* 清空输入框内容 */
        //$("#input-tags").val("");

        /* 插入tag到隐藏的input框 */
        updateSubmitTags();
    }

    function initShowTags() {
        var tags = $('#submit-tags').val().split(",");
        insertShowTags(tags);
    }
    initShowTags();

    $(document).on("click",".add-tags-button",function() {
    });

    document.addEventListener('click', function(event) {
        var div = document.getElementById('tags-autocomplete-select-menu');
        var divAddTags = document.getElementById('add-tags-button');
        if (!tagsMenuShow && divAddTags == event.target) {
            $("#input-tags").focus();
            searchTags($(this).val().replace(/(^\s*)|(\s*$)/g, ""));
            tagsSelectMenuShow(true);
        } else if (tagsMenuShow){
            if (event.target !== div && !div.contains(event.target)) {
                tagsSelectMenuShow(false);
                $(".tags-autocomplete-select-menu").fadeOut();
            }
        }
    });

    $("#input-tags").focus(function(){
        searchTags($(this).val().replace(/(^\s*)|(\s*$)/g, ""));
    });

    $("#input-tags").bind('input propertychange',function(e){
        searchTags($(this).val().replace(/(^\s*)|(\s*$)/g, ""));
        var word = $(this).val();
        var pos = word.indexOf(",");
        if (pos > 0) {
            insertTags(word);
        }
    });

    $(document).on("click",".tags-autocomplete-select-item",function() {
        insertTags($(this).attr("data-value"));
    });
    $(document).on("click",".icon-delete",function() {
        $(this).parent().remove();
        updateSubmitTags();
    });

});