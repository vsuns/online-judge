
var IE6 = (navigator.userAgent.indexOf("MSIE 6")>=0) ? true : false;
if(IE6){
	$(function(){
		$("<div><p><br /><strong>Sorry! This page doesn't support Internet Explorer 6.</strong><br /><br />If you'd like to read our content please <a href='http://getfirefox.org'>upgrade your browser</a>.</p>"
				+"<p><br /><strong>You can use chrome , firefox or IE7! Thanks for your understanding!</strong></p>"
		)
		.css({
			backgroundColor: '#f8f8f8',
			'top': '50%',
			'left': '50%',
			marginLeft: -210,
			marginTop: -100,
			width: 410,
			paddingRight: 10,
			height: 200,
			'position': 'absolute',
			zIndex: 6000
		})
		.appendTo("body");
	});	
};

function getOs()   
{   
    var OsObject = "";   
    if(navigator.userAgent.indexOf("MSIE")>0) {   
         return "MSIE";   
    }   
    if(isFirefox=navigator.userAgent.indexOf("Firefox")>0){   
         return "Firefox";   
    }   
    if(isSafari=navigator.userAgent.indexOf("Safari")>0) {   
         return "Safari";   
    }    
    if(isCamino=navigator.userAgent.indexOf("Camino")>0){   
         return "Camino";   
    }   
    if(isMozilla=navigator.userAgent.indexOf("Gecko/")>0){   
         return "Gecko";   
    }   
 }   

function html_encode(str)   
{   
  var s = "";   
  if (str.length == 0) return "";   
  s = str.replace(/&/g, "&amp;");   
  s = s.replace(/</g, "&lt;");   
  s = s.replace(/>/g, "&gt;");   
  //s = s.replace(/ /g, "&nbsp;");   
  s = s.replace(/\'/g, "&#39;");   
  s = s.replace(/\"/g, "&quot;");   
  //s = s.replace(/\n/g, "<br>");   
  return s;   
}   

function html_decode(str)   
{   
  var s = "";   
  if (str.length == 0) return "";   
  s = str.replace(/&gt;/g, "&");   
  s = s.replace(/&lt;/g, "<");   
  s = s.replace(/&gt;/g, ">");   
  s = s.replace(/&nbsp;/g, " ");   
  s = s.replace(/&#39;/g, "\'");   
  s = s.replace(/&quot;/g, "\"");   
  s = s.replace(/<br>/g, "\n");   
  return s;   
}  

function OnlineJudge(){
	this.saveUrl = function(){
	}
	
    this.setCookie = function(name, value, exdays) {
    	var d = new Date();
    	d.setTime(d.getTime()+(exdays*24*60*60*1000));
    	var expires = "expires="+d.toGMTString();
        document.cookie = name + "=" + escape(value) + "; " + expires;
    };

    this.getCookie = function(name) {
        var prefix = name + "=";
        var from = document.cookie.indexOf(prefix);
        if (from < 0) {
            return null;
        } else {
            var to = document.cookie.indexOf(";", from + prefix.length);
            if (to < 0) {
                to = document.cookie.length;
            }
            return unescape(document.cookie.substring(from + prefix.length, to));
        }
    };
	this.pingEx = function(interval, count) {
		if (count < 1000){
			$.post("/api/ping", function(result){}, "json");
			setTimeout(function () { OnlineJudge.pingEx(interval * 2, count + 1); }, interval);
		}
	}
	this.ping = function() {
	     var count = 0;
	     var interval = 30000;
	     $.post("/api/ping", function(result){}, "json");
		 setTimeout(function () { OnlineJudge.pingEx(interval * 2, count + 1); }, interval);
    };
    
    this.updateMails = function(seconds, count){
    	if (0 == seconds%60) {
    		$.post("/api/mail/new-mail-count", function(json) {
    	        if (json.success == true) {   
    	        	count = json.nCount;   
    	        	$(".new-mail-count").html("("+count+")");         
    	        }	
    	    }, "json");
    		seconds = 1;
    	}
        if (count>0) {
            if (0 == seconds%2) {
       			$("#mails_nav").attr('src', 'img/mail_unread.png');
    	   	} else{
    	   		$("#mails_nav").attr('src', 'img/mail.png');
    	   	}
        }
    	setTimeout(function(){OnlineJudge.updateMails(seconds + 1, count);}, 1000);
    }

    this.statusReflesh = function(){
    	var loader = "&nbsp; <img alt='Loading...' src='/img/loader.gif' style='vertical-align: middle;'/>";
    	a = new Array();
    	for(var i = 1; i < 5; i++){
    		if(i != 3){
    			 var results = $('.verdict[status='+i+'][manual=0]').each(function(i, el){
    	             var zz = $(el).attr('id').substring(7);
    	             a.push( zz );
    	     	 });
    		} 
    	};
    	if( a.length > 0 ){
    		$.ajax({type : "POST",
				url : "/api/status/query",
				data : "ids=" + a.join(','),
				dataType : 'json',
				success : function(result) {
					var solutions = result.data;
					for ( var i = 0; i < solutions.length; i++) {
    					$("#status_" + solutions[i].solution_id).attr('status',
							solutions[i].verdictId);
    					$("#time_" + solutions[i].solution_id).html(
							solutions[i].time + " ms");
    					$("#memory_" + solutions[i].solution_id).html(
							solutions[i].memory + " kb");

    					if (solutions[i].verdict == 5) {
    						$("#status_" + solutions[i].solution_id).attr('class', 'verdict verdict_ac');
    					} else if (solutions[i].verdict == 3) {
							$("#status_" + solutions[i].solution_id).attr('class', 'verdict verdict_ce');
						} else if (solutions[i].verdict == 10) {
    						$("#status_" + solutions[i].solution_id).attr('class', 'verdict verdict_pe');
    					} else if (solutions[i].verdict < 5) {
    						$("#status_" + solutions[i].solution_id).attr('class', 'verdict verdict_prev');
    					} else {
							$("#status_" + solutions[i].solution_id).attr('class', 'verdict verdict_other');
						}

						$("#a_status_" + solutions[i].solution_id).html(solutions[i].status_description);
						$("#time_" + solutions[i].solution_id).html(solutions[i].time + ' ms');
						$("#memory_" + solutions[i].solution_id).html(solutions[i].memory + ' kb');
						$("#status_" + solutions[i].solution_id).attr('status', solutions[i].verdict);
    				}

    				window.setTimeout(OnlineJudge.statusReflesh, 5000);
    			}
    		});
    	}
    }
    
    this.reload = function() {
        window.location.reload();
    };

    this.redirect = function(link) {
        window.location = link;
    };
	this.timeFormatSeconds = function (time) {
		var d = time ? new Date(time) : new Date();
		var year = d.getFullYear();
		var month = d.getMonth() + 1;
		var day = d.getDate();
		var hours = d.getHours();
		var min = d.getMinutes();
		var seconds = d.getSeconds();

		if (month < 10) month = '0' + month;
		if (day < 10) day = '0' + day;
		if (hours < 10) hours = '0' + hours;
		if (min < 10) min = '0' + min;
		if (seconds < 10) seconds = '0' + seconds;
		return (year + '-' + month + '-' + day + ' ' + hours + ':' + min + ':' + seconds);
	}
    this.parseSeconds = function (seconds) {
        var ap = 0;
        if (seconds.length == 9) {
            ap = seconds.charAt(0) - '0';
            seconds = seconds.substring(1);
        }
        if (seconds.length == 8 && seconds.charAt(2) == ':' && seconds.charAt(5) == ':') {
            var s = (seconds.charAt(7) - '0') + 10 * (seconds.charAt(6) - '0');
            var m = (seconds.charAt(4) - '0') + 10 * (seconds.charAt(3) - '0');
            var h = (seconds.charAt(1) - '0') + 10 * (seconds.charAt(0) - '0') + 100 * ap;
            return s + 60 * m + 60 * 60 * h;
        } else {
            return 0;
        }
    };

    this.formatSS = function(seconds) {
        if (seconds <= 9) {
            return "0" + seconds;
        } else {
            return seconds;
        }
    };

    this.formatSeconds = function(seconds) {
        var s = seconds % 60;
        seconds = (seconds - s) / 60
        var m = seconds % 60;
        var h = (seconds - m) / 60
        return OnlineJudge.formatSS(h) + ":" + OnlineJudge.formatSS(m) + ":" + OnlineJudge.formatSS(s);
    };
	this.formatMinutes = function(seconds) {
		var s = seconds % 60;
		seconds = (seconds - s) / 60
		var m = seconds % 60;
		var h = (seconds - m) / 60
		return OnlineJudge.formatSS(h) + ":" + OnlineJudge.formatSS(m);
	};
    this.countdown = function () {
        var now = new Date().getTime();
        var e = $(".countdown");
        var starts = new Array();
       
        var index = 0;
        e.each(function () {
            index++;
            $(this).attr("cdid", "i" + index);
        });

        var callback = function () {
            e.each(function () {
                var textBeforeRedirect = $(this).attr("textBeforeRedirect");
                var redirectUrl = $(this).attr("redirectUrl");
                var home = $(this).attr("home");
                var noRedirection = $(this).attr("noRedirection");

                var id = $(this).attr("cdid");
                var txt = $(this).text();
                var s = OnlineJudge.parseSeconds(txt);
                if (s > 0) {
                    if (starts[id] == undefined)
                        starts[id] = s;
                    var passed = Math.floor((new Date().getTime() - now) / 1000);
                    var val = starts[id] - passed;
                    if (val >= 0) {
                        $(this).text(OnlineJudge.formatSeconds(val));
                    }
                    if (val <= 0) {
                        if (noRedirection != "true" && noRedirection != "yes") {
                            if (textBeforeRedirect) {
                            	OnlineJudge.alert(textBeforeRedirect, function () {
                                    if (redirectUrl) {
                                        window.setTimeout(OnlineJudge.redirect(redirectUrl), Math.floor(Math.random() * 2000));
                                    } else {
                                        window.setTimeout(OnlineJudge.reload, Math.floor(Math.random() * 2000));
                                    }
                                });
                            } else {
                                if (redirectUrl) {
                                    window.setTimeout(OnlineJudge.redirect(redirectUrl), Math.floor(Math.random() * 5000));
                                } else {
                                    window.setTimeout(OnlineJudge.reload, Math.floor(Math.random() * 5000));
                                }
                            }
                        } else {
                            if (textBeforeRedirect) {
                            	OnlineJudge.alert(textBeforeRedirect);
                            }
                        }
                    }
                }
            });
            window.setTimeout(callback, 1000);
        };
        window.setTimeout(callback, 0);
    };

	this.parseFriendlyDuration = function(duration) {
		if(duration < 172800){
			return OnlineJudge.formatMinutes(duration);
		}
		if (Math.floor(duration / 86400) === 1) {
			return "1 day";
		}
		return Math.floor(duration / 86400) + " days";
	};
	this.parseFriendlyTimeLeft = function(timeLeft) {
		if(timeLeft < 172800){
			return OnlineJudge.formatSeconds(timeLeft);
		}
		if (Math.floor(timeLeft / 86400) === 1) {
			return "1 day";
		}
		return Math.floor(timeLeft / 86400) + " days";
	};
	this.countdownEx = function () {
		var now = new Date().getTime();
		var e = $(".countdown");
		var starts = new Array();

		var index = 0;
		e.each(function () {
			index++;
			$(this).attr("cdid", "i" + index);
		});

		var callback = function () {
			e.each(function () {
				var id = $(this).attr("cdid");
				var txt = $(this).text();
				var s = OnlineJudge.parseSeconds(txt);
				if (s > 0) {
					if (starts[id] == undefined)
						starts[id] = s;
					var passed = Math.floor((new Date().getTime() - now) / 1000);
					var val = starts[id] - passed;
					if (val >= 0) {
						$(this).text(OnlineJudge.parseFriendlyTimeLeft(val));
					}
				}
			});
			window.setTimeout(callback, 1000);
		};
		window.setTimeout(callback, 0);
	};
    this.ratingGragh = function(username) {
    	$.get("/api/rating/"+username, function(json_data) {
    		var data = new Array();
    		var rating = new Array();
    		var rating_min = 99999;
    		var rating_max = 0;
    		var rating_max_date = 0;
    		if (json_data.data != null) {
    			for(var i = json_data.data.length - 1; i >= 0 ; i--) {
    					if (json_data.data[i].rating < rating_min) {
    						rating_min = json_data.data[i].rating;
    					}
    					if (json_data.data[i].rating > rating_max) {
    						rating_max = json_data.data[i].rating;
    						rating_max_date = Date.parse(new Date(json_data.data[i].rating_date.replace("T", " ")));
    					}
    					rating.push([
    			            Date.parse(new Date(json_data.data[i].rating_date.replace("T", " "))),
    			            json_data.data[i].rating,
    			            json_data.data[i].delta,
    			            json_data.data[i].contest_id,
    			            json_data.data[i].contest_name,
    			            json_data.data[i].rank,
    			            json_data.data[i].rate,
    			            json_data.data[i].rating_title,
    			            json_data.data[i].rating_date.replace("T", " "),
    			        ]);
    			}
    		}
    		data.push(rating);
    		data.push([
    	        [
    	            rating_max_date, rating_max
    	        ]
    	    ]);
    	    
    		var datas = [
    		    {label: username, data: data[0]},
    		    {clickable: false, hoverable: false, color: "red", data: data[1]}
    		];
    		
    		var markings = [
    		    { color: '#a00', lineWidth: 1, yaxis: { from: 3000 } },
    		    { color: 'red', lineWidth: 1, yaxis: { from: 2600, to: 2999 } },
    		    { color: '#FFD700', lineWidth: 1, yaxis: { from: 2200, to: 2599 } },
    		    { color: '#FF8C00', lineWidth: 1, yaxis: { from: 2050, to: 2199 } },
    		    { color: '#FF1493', lineWidth: 1, yaxis: { from: 1900, to: 2049 } },
    		    { color: '#f8f', lineWidth: 1, yaxis: { from: 1750, to: 1899 } },
    		    { color: '#aaf', lineWidth: 1, yaxis: { from: 1600, to: 1749 } },
    		    { color: '#77ddbb', lineWidth: 1, yaxis: { from: 1400, to: 1599 } },
    		    { color: '#7f7', lineWidth: 1, yaxis: { from: 1200, to: 1399 } },
    		    { color: '#ccc', lineWidth: 1, yaxis: { from: 0, to: 1199 } },
    		];
    		
    		var options = {
    		    lines: { show: true },
    		    points: { show: true },
    		    xaxis: { mode: "time" },
    		    yaxis: { min: (rating_min == 99999) ? 1000 : (rating_min - rating_min % 100 - 300), 
    		    		 max: (rating_max == 0) ? 2000 : (rating_max + 300 - rating_max%100), 
    		    		 ticks: [1200, 1400, 1600, 1750, 1900, 2050, 2200, 2600, 3000] },
    		    grid: { hoverable: true, markings: markings }
    		};
    		
    		var plot = $.plot($("#placeholder"), datas, options);
    		
    		function showTooltip(x, y, contents) {
    		    $('<div id="tooltip">' + contents + '</div>').css( {
    		        position: 'absolute',
    		        display: 'none',
    		        top: y - 20,
    		        left: x + 10,
    		        border: '1px solid #fdd',			        
    		        padding: '2px',
    		        'line-height': '16px',
    		        'font-size' : '11px',
    		        'background-color': '#fee',
    		        opacity: 0.80
    		    }).appendTo("body").fadeIn(200);
    		}
    		
    		var ctx = plot.getCanvas().getContext("2d");
    		
    		var prev = -1;
    		$("#placeholder").bind("plothover", function (event, pos, item) {
    		    if (item) {
    		        if (prev != item.dataIndex) {
    		            $("#tooltip").remove();
    		            var params = data[item.seriesIndex][item.dataIndex];            
    		            var total = params[1];
    		            var change = params[2] > 0 ? "+" + params[2] : params[2];
    		            var contestName = params[4];
    		            var contestId = params[3];
    		            var contestUrl = "/contest/" + contestId + "/standings";
    		            var rank = params[5];
    		            var rate = params[6];
    		            var title = params[7];
    		            var time = params[8];
    		            var html = "= " + total + " (" + change + "), " + "<span class='rated-user user-rate-" + rate + "'>" + title + "</span>" + "<br/>"
    		                            + "Rank: " + rank + "<br/>"
    		                            + "<a href='" + contestUrl + "' title=' at "+ time + "'>" + contestName + "</a>";

    		            showTooltip(item.pageX, item.pageY, html);
    		            setTimeout(function () {
    		                $("#tooltip").fadeOut(200);
    		                prev = -1;
    		            }, 4000);
    		            prev = item.dataIndex;
    		        }
    		    }
    		});	
    	},"json");
    };

	this.userProblemTry = function(username) {
		function getUserProblemTry(username, type) {
			var opt="";
			$.getJSON("/api/solution/try",{username: username, type: type},function(json){
				if (json.code != 200) {
					return false;
				}
				var data = json.data;
				for ( var i = 0; i < data.length; i++) {
					opt+= "<span title='" + data[i].title + "'>";
					opt+= "<a class='" + type + "' ";
					opt+= "href='/problemset/" + data[i].problemId + "/status/" + username + "/page/1'>";
					opt+= data[i].problemId;
					opt+= "</a></span>"
					opt+= " "
				}
				$("span.problem_"+type+"_num").html(data.length);
				$("div.problem_"+type).html(opt);
			});
		}

		getUserProblemTry(username, "solved");
		getUserProblemTry(username, "try");
	}

	this.tagTip = function() {
		$("a.tag").poshytip({
			className: 'tip-whitesimple',
			alignTo: 'target',
			alignX: 'right',
			alignY: 'bottom',
			offsetX: 5,
			offsetY: -45,
			content: function(updateCallback) {
				$.post("/api/tags/query", { q:$(this).attr('tag')} ,function(data){
					if (data.code != 200) {
						return true;
					}
					var tag = data.data.tag;
					var buf = "<div class=\"tag-tip\">"
						+ "<div class=\"tag-header\">"
						+ "<div class=\"tags\">"
						+ "<span><a>" + tag.name + "</a></span>"
						+ "</div>"
						+ "</div>";
					buf += "<div class=\"tag-description\" tagname=\""+tag.name+"\">"
						+ "<span>" + tag.description + "</span>"
						+ "</div>";
					buf += "<div class=\"tag-stat\">"
						+ "<div><a href='/problemset?tag="+ tag.name + "'>" + tag.problem_count + "</a> "
						+ data.data.tag_problems_name + " , "
						+ "<a href='/topics?tag="+ tag.name + "'>" + tag.topic_count + "</a> "
						+ data.data.tag_topics_name + " , "
						+ data.data.tag_post_name + "<span title=\""+ tag.indate +"\">"+tag.createFriendlyDate+"</span>"
						+ "</div></div></div>";
					updateCallback(buf);
				});
				return $(this).html();
			}
		});
	};
	this.userTip = function() {
		$(".user-tip").poshytip({	
			className: 'tip-whitesimple',
			alignTo: 'target',		
			alignX: 'right',
			alignY: 'bottom',
			offsetX: 5,
			offsetY: -45,
			content: function(updateCallback) {
				$.post("/api/userInfo", { username:$(this).attr('user')} ,function(data){
					var user = data.data.user
					var buf = "<div class=\'UserOutline\'>"
					buf += "<div class='u'>";
					buf += "<a href='/profile/"+ user.username + "'>";
					buf += "<img class='normal-avatar' src='" + user.avatar + "' alt='" + user.username + "'>";
					buf += "</a>";
					buf += "</div>"

					buf+="<h1><a href='/mails/new/"+ user.username + "' class='rated-user user-rate-"+ user.rate+"'>@"+user.username+"</a></h1>" +
						"<span class='city'>";
					buf+= user.school;
					buf+="</span>";
					buf+="<div id='UserMotto'>";
					buf+= user.motto;
					buf+="</div><div style='clear:both'></div>";

					buf+="<div id='UserStat'>"+
						"<ul>"+
						"<li><em>"+ user.rank +"</em>"+ data.data.rank_name + "</li>"+
						"<li><a href='/contests/" + user.username +"'><em>"+ user.rating +"</em>"+ data.data.rating_name +"</a></li>"+
						"<li><a href='/problemset/status/"+user.username+"/page/1'><em>"+user.solved+"</em>"+ data.data.solved_name +"</a></li>"+
						"<li style='border-right:0;'><a href='/topics'><em>"+ data.data.topic_count +"</em>"+data.data.topic_name+"</a></li>"+
						"</ul>"+
						"</div>"+
						"<div class='opts'></div>"+
						"<div id='Logs'>"+
						"<strong>"+ data.data.recent_action_name +"</strong>"+
						"<div class='log'>";
						var topics = data.data.topic
						for ( var i = 0; i < topics.length; i++) {
							var url = "";
							var img = "";
							if(topics[i].parent_id > 0){
								url="/topic/"+topics[i].root_id+"#rpl_"+topics[i].message_id;
								img = "<img src='/img/comment-12.png' title='New comment(s)'>";
							}else{
								url="/topic/"+topics[i].message_id;
								img = "<img src='/img/x-update-12x12.png' title='New or modify topic'>";
							}
							buf += img + "<a href='"+url+"'>"+topics[i].title+"</a>"+" :<span style='color:grey'>"+ topics[i].content_abstract +"</span></br>";
						}
						if (topics.length == 0) {
							buf+="<span style='color:grey'>"+data.data.no_any_action_name+"</span>";
						}
						buf+="</div>"+
							"</div>"+
							"</div>";
					updateCallback(buf);
				});
				return "Loading...";		
			}
		});	
	};	

	this.onlineUsers = function() {
		$.getJSON("/api/online-users", function(json) {
	        if (json.code != 200) {
		        return;
	        }
	        var opt="";
			for ( var i = 0; i < json.data.length; i++) {
				opt+="<b><a title='" + json.data[i].username + ", "+ json.data[i].levelTitle  +
						", " + json.data[i].lastAccessTime +
						"' href='/profile/"+ json.data[i].username +
						"' class='rated-user user-rate-" + json.data[i].level + " user-tip'" +
						" user='" + json.data[i].username + "'>" +
					json.data[i].nickname + "</a></b> ";
			}
			$("div.sidebar-online-users").html(opt);
	   },"json");
	};

	this.prettyEditBox = function() {
		$(".pretty-edit-box-text").on("input propertychange", function() {
			var len = $(this).val().length;
			var childElements = $(this).parent().find('.takeTip');
			childElements.each(function() {
				$(this).text(len);
			});
		});

		$(".pretty-edit-box-text").focusin(function() {
			$(this).parent().addClass("pretty-edit-box-focus")
			var childElements = $(this).parent().find('.pretty-edit-box-tip');
			childElements.each(function() {
				$(this).css("display", "inline");
			});

			var len = $(this).val().length;
			var childElementsTip = $(this).parent().find('.takeTip');
			childElementsTip.each(function() {
				$(this).text(len);
			});
		});
		$(".pretty-edit-box-text").focusout(function() {
			var childElements = $(this).parent().find('.pretty-edit-box-tip');
			childElements.each(function() {
				$(this).css("display", "none");
			});
			$(this).parent().removeClass("pretty-edit-box-focus");
		});
	}
	jQuery.fn.extend({
		autoHeight: function(){
			return this.each(function(){
				var $this = jQuery(this);
				if( !$this.attr('_initAdjustHeight') ){
					$this.attr('_initAdjustHeight', $this.outerHeight());
				}
				_adjustH(this).on('input', function(){
					_adjustH(this);
				});
			});
			function _adjustH(elem){
				var $obj = jQuery(elem);
				return $obj.css({
					height: $obj.attr('_initAdjustHeight'),
					'overflow-y': 'hidden'
				}).height( elem.scrollHeight );
			}
		}
	});
	this.prettyTitle = function() {
		/*
		var oldTitle = null
		$(document).bind('mouseover mouseout mousemove', function (event) {
			var left = event.pageX, top = event.pageY, title = event.target.title, type = event.originalEvent.type;
			if (title == null) {
				return true;
			}
			if (type === 'mouseover') {
				oldTitle = title;
				if (title.length > 0) {
					event.target.title = '';
					var ele = $('<div></div>', {text: title, class: 'title-class'}).css({
						position: 'absolute',
						top: top + 10,
						left: left + 10,
						padding: '5px 10px',
						textAlign: 'left',
						fontSize: '12px',
						lineHeight: '13px',
						color: '#000',
						border: '1px solid #e1e1e1',
						background: '#fff',
						borderRadius: '5px'
					});
					ele.appendTo('body')
				}
			} else if (type === 'mouseout') {
				if (oldTitle != null) {
					event.target.title = oldTitle;
				}
				$('.title-class').remove();
			} else if (type === 'mousemove') {
			}
		})*/
	}
}
OnlineJudge = new OnlineJudge();
$(document).ready(function () {
	$(document).on("click",".latest_news_item",function() {
		var url = $(this).attr("url");
		window.open(url, '_blank');
		return false;
	});
});

