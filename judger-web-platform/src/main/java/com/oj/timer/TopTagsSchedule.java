package com.oj.timer;

import com.oj.entity.User;
import com.oj.service.TagsService;
import com.oj.service.UserService;
import com.oj.util.MyFreeMarker;
import freemarker.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Component
public class TopTagsSchedule {
    @Autowired
    FreeMarkerConfigurer freeMarkerConfigurer;

    @Autowired
    private TagsService tagsService;

    @PostConstruct
    @Scheduled(fixedDelay = 24*60*60*1000)
    private void topTagsTimer() {
        tagsService.topTagsGenerator();
    }
}

