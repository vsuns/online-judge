package com.oj.timer;

import com.oj.service.MessageService;
import com.oj.util.OJCollector;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Slf4j
@Component
public class ContestCollectorSchedule {
    @Scheduled(cron = "0 0 1,6,12,18,21 * * ?")
    private void contestCollectorTimer() {
        try {
            OJCollector.CollectContests();
        }catch (Exception e) {
            log.error("CollectContests Exception" + e.toString());
        }
    }

    /* 启动后延时1分钟执行一次，后续按照上面的定时器调度 */
    @Scheduled(initialDelay = 30000, fixedRate = 365 * 24 * 60 * 60 *1000)
    private void contestCollectorTimerStartup() {
        try {
            OJCollector.CollectContests();
        }catch (Exception e) {
            log.error("CollectContests Exception" + e.toString());
        }
    }
}
