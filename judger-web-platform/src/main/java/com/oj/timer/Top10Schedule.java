package com.oj.timer;

import com.oj.entity.User;
import com.oj.service.UserService;
import com.oj.service.impl.I18nService;
import com.oj.util.MyFreeMarker;
import com.oj.util.Result;
import freemarker.template.Template;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class Top10Schedule {
    @Autowired
    FreeMarkerConfigurer freeMarkerConfigurer;
    @Autowired
    private UserService userService;
    @Autowired
    private I18nService i18nService;
    @Scheduled(fixedDelay = 24*60*60*1000)
    private void top10Timer() {
        try {
            List<User> users = userService.topUsers();
            if (users == null) {
                return;
            }

            Map map = new HashMap();
            map.put("topusers", users);
            Template template = freeMarkerConfigurer.getConfiguration().getTemplate("freemarker/topusers.ftl");
            MyFreeMarker.generator(template, "topusers.html",map);
            log.info("topusers generator finish.");
        }catch (Exception e) {
            log.error("topusers exception: " + e.toString());
        }
    }
}
