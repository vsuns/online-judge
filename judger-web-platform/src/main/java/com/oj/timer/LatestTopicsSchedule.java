package com.oj.timer;

import com.oj.entity.Message;
import com.oj.mapper.MessageMapper;
import com.oj.service.MessageService;
import com.oj.service.UserService;
import com.oj.util.MyFreeMarker;
import freemarker.template.Template;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class LatestTopicsSchedule {
    @Autowired
    private MessageService messageService;

    @PostConstruct
    @Scheduled(cron = "0 0 1,6,12,18,21 * * ?")
    private void latestTopicsTimer() {
        messageService.topTopicsGenerator();
    }
}
