package com.oj.exception;

import com.oj.util.CodeMsg;

public class JsonOJException extends RuntimeException{
    private CodeMsg codeMsg;

    public JsonOJException(CodeMsg codeMsg) {
        super(codeMsg.toString());
        this.codeMsg = codeMsg;
    }

    public CodeMsg getCodeMsg() {
        return this.codeMsg;
    }
}
