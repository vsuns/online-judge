package com.oj.exception;

import com.oj.util.CodeMsg;
import org.springframework.context.i18n.LocaleContextHolder;

public class OJException extends RuntimeException{
    private CodeMsg codeMsg;

    public OJException(CodeMsg codeMsg) {
        super(codeMsg.toString());
        this.codeMsg = codeMsg;
    }

    public CodeMsg getCodeMsg() {
        return this.codeMsg;
    }
}
