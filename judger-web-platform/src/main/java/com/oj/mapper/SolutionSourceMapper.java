package com.oj.mapper;

import com.oj.entity.Solution;
import com.oj.entity.SolutionSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SolutionSourceMapper {
    Integer insert(SolutionSource solutionSource);

    List<SolutionSource> queryList();

    SolutionSource query(Integer solutionId);
}