package com.oj.mapper;

import com.github.pagehelper.Page;
import com.oj.entity.Message;
import com.oj.entity.Tags;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TagsMapper {
    Tags query(Integer id);

    Tags queryByName(String name);

    List<Tags> search(String q);

    boolean update(Tags tags);

    boolean insert(Tags tags);

    boolean delete(Integer tagId);

    Page<Tags> findByPaging();
}