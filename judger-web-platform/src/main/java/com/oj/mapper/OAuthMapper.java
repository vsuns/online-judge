package com.oj.mapper;

import com.oj.entity.OAuth;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface OAuthMapper {
    Integer save(OAuth oauth);
    void delete(Integer id);
    List<OAuth> queryByUser(String username);
    OAuth queryOAuthByUser(String provider, String username);
    OAuth queryOAuthByLoginName(String provider, String login);
}