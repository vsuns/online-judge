package com.oj.mapper;

import com.github.pagehelper.Page;
import org.springframework.stereotype.Repository;
import com.oj.entity.User;

import java.util.List;
import java.util.Map;

//@Mapper
@Repository
public interface UserMapper {
    List<User> search(Map param);

    List<User> queryList();

    List<User> topUsers();

    User query(String username);
    User queryMail(String email);

    boolean update(User user);

    Integer insertUser(User user);

    Page<User> findByPaging();

    Page<User> findByPagingAdmin();

    Integer getUserRank(User user);
}