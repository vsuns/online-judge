package com.oj.mapper;

import com.oj.entity.Solution;
import com.github.pagehelper.Page;
import com.oj.entity.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface SolutionMapper {
    Integer insert(Solution solution);

    List<Solution> queryList();

    Solution query(Integer solutionId);

    List<Solution> queryList(String username, Integer problemId, Integer contestId, Integer fromSolutionId);

    Page<Solution> findByPaging(Map param);

    List<Integer> queryUserProblem(Map param);

    Integer getProblemTriedCount(String username, Integer problemId, Integer contestId);

    Integer getProblemSolvedCount(String username, Integer problemId, Integer contestId);
}