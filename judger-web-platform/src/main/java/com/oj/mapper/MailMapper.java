package com.oj.mapper;

import com.github.pagehelper.Page;
import com.oj.entity.Mail;
import com.oj.entity.Message;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface MailMapper {
    boolean insert(Mail mail);

    boolean update(Mail mail);

    Mail query(Integer id);

    Page<Mail> getMailsSend(String username);

    Page<Mail> getMailsRecv(String username);
}