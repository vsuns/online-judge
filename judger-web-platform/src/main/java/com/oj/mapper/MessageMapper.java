package com.oj.mapper;

import com.github.pagehelper.Page;
import com.oj.entity.Message;
import com.oj.entity.User;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface MessageMapper {
    boolean insert(Message message);

    boolean update(Message message);

    Message query(Integer messageId);

    List<Message> queryReplys(Integer messageId);

    List<Message> queryChildReplys(Integer messageId);

    Page<Message> findByPaging(Map param);

    List<Message> queryList(Map param);

    List<Message> search(Map param);

    Integer getUserTopicsCount(String username);
}