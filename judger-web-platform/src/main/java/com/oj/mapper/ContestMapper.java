package com.oj.mapper;

import com.github.pagehelper.Page;
import com.oj.entity.Contest;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface ContestMapper {
    boolean insert(Contest contest);
    boolean update(Contest contest);
    List<Contest> queryList();
    Contest query(Integer contestId);
    Contest queryAdmin(Integer contestId);
    Page<Contest> findByPaging(Map param);
    Page<Contest> findByPagingAdmin(Map param);
}