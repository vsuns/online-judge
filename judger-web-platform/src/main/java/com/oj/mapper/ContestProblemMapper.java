package com.oj.mapper;

import com.oj.entity.ContestProblem;
import com.oj.entity.Problem;

import java.util.List;

public interface ContestProblemMapper {
    boolean insert(ContestProblem contestProblem);
    boolean delete(Integer contestId, String num);
    List<ContestProblem> queryByContest(Integer contestId);

    ContestProblem query(Integer contestId, String num);
    ContestProblem queryByProblemId(Integer contestId, Integer problemId);
}
