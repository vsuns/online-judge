package com.oj.mapper;

import com.github.pagehelper.Page;
import com.oj.entity.Message;
import com.oj.entity.Problem;
import com.oj.entity.Solution;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface ProblemMapper {
    boolean insert(Problem problem);

    boolean update(Problem problem);

    List<Problem> query(Integer problemId);

    Page<Problem> findByPaging(Map param);

    Problem queryAdmin(Integer problemId);

    List<Problem> searchAdmin(String search);

    List<Problem> search(Map param);
}