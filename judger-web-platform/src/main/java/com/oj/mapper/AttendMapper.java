package com.oj.mapper;

import com.github.pagehelper.Page;
import com.oj.entity.Attend;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface AttendMapper {
    List<Attend> queryList(Map param);

    Attend query(Integer contestId, String username);

    void delete(Integer id);

    Page<Attend> findByPaging(Integer contestId, Integer type);

    Integer getUserRank(Map param);

    Page<Attend> getRegistrantsByPaging(Integer contestId);

    Integer insert(Integer contestId, String username);
}