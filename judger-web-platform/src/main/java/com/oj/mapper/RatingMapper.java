package com.oj.mapper;

import com.oj.entity.Rating;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RatingMapper {
    List<Rating> queryList(String username);

    Rating query(Integer contestId, String username);

}