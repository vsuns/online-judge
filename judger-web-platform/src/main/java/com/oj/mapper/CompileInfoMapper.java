package com.oj.mapper;

import com.github.pagehelper.Page;
import com.oj.entity.CompileInfo;
import com.oj.entity.Mail;
import org.springframework.stereotype.Repository;

@Repository
public interface CompileInfoMapper {
    CompileInfo query(Integer solutionId);
}