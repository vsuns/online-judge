package com.oj.mapper;

import com.oj.entity.Privilege;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PrivilegeMapper {
    Privilege query(String username, String rightstr);
}