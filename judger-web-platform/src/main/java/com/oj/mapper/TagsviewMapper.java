package com.oj.mapper;

import com.github.pagehelper.Page;
import com.oj.entity.Rating;
import com.oj.entity.Tags;
import com.oj.entity.Tagsview;
import com.oj.entity.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TagsviewMapper {
    List<Tagsview> queryByProblems(Integer problemId);
    List<Tagsview> queryByMessages(Integer messageId);

    boolean deleteByProblemId(Integer problemId);

    boolean deleteByMessageId(Integer messageId);

    boolean insert(Tagsview tagsView);

    boolean delete(Integer id);

    Page<Tagsview> findByPaging();

    List<Tagsview> queryList();

    List<Tagsview> queryByTagId(Integer tagId);
    List<Tagsview> queryProblemsByTagId(Integer tagId);
    List<Tagsview> queryTopicsByTagId(Integer tagId);
}