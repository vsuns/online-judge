package com.oj.controller;

import com.oj.config.OAuthConfig;
import com.oj.config.OJConfig;
import com.oj.entity.User;
import com.oj.service.UserService;
import com.oj.util.MD5Util;
import com.oj.util.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

@Slf4j
@Controller
@RequestMapping
public class UploadController {
    @Autowired
    private UserService userService;

    @ResponseBody
    @PostMapping(value = "/api/upload/image")
    public Result<String> uploadImage(@RequestParam("file") MultipartFile file, HttpServletRequest request){
        try {
            if (file.isEmpty()){
                return Result.error(0, null);
            }
            String username = request.getSession().getAttribute("session_username").toString();
            if (username == null) {
                return Result.error(0, null);
            }
            User user = userService.query(username);
            if (user == null) {
                return Result.error(0, null);
            }

            ApplicationHome h = new ApplicationHome(getClass());
            File jarF = h.getSource();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            String ymd = sdf.format(new Date());
            String appPath = jarF.getParentFile().toString();
            String directory = "upload" + File.separator + "image"+ File.separator + ymd;
            File uploadFileSaveDir = new File(appPath + File.separator + directory);
            if(!uploadFileSaveDir.exists()){
                uploadFileSaveDir.mkdirs();
            }

            SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
            String fileName = df.format(new Date()) + "_" + MD5Util.MD5(username+df.format(new Date())+ new Random().nextInt(1000)) + ".png";
            String filePath = appPath + File.separator + directory + File.separator + fileName;
            File dest = new File(filePath);
            try {
                file.transferTo(dest);
                log.info("upload success : " + File.separator + directory + File.separator + fileName);
                return Result.success(File.separator + directory + File.separator + fileName);
            } catch (IOException e) {
                return Result.error(0, null);
            }
        }catch (Exception e) {
            return Result.error(0, null);
        }
    }


    @ResponseBody
    @PostMapping(value = "/api/admin/upload/testcases")
    public Result<String> uploadTestcases(@RequestParam("problemId") Integer problemId,
                                          @RequestParam("testcases") MultipartFile file,
                                          HttpServletRequest request){
        try {
            if (file.isEmpty()){
                return Result.error(0, null);
            }

            String basePath = OJConfig.getDataPath() + File.separator + problemId.toString();
            File basePathDir = new File(basePath);
            if(!basePathDir.exists()){
                basePathDir.mkdirs();
            }
            String filePath = basePath + File.separator + file.getOriginalFilename();
            File dest = new File(filePath);
            try {
                file.transferTo(dest);
                log.info("upload testcases success : " + filePath);
                return Result.success(file.getOriginalFilename());
            } catch (IOException e) {
                log.error("upload error : " + filePath);
                return Result.error(0, null);
            }
        }catch (Exception e) {
            log.error("upload error");
            return Result.error(0, null);
        }
    }
}