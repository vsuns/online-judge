package com.oj.controller;

import com.oj.entity.Rating;
import com.oj.entity.User;
import com.oj.service.RatingService;
import com.oj.service.UserService;
import com.oj.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping
public class RatingController {
    @Autowired
    private RatingService ratingService;

    @ResponseBody
    @RequestMapping(value = {"/api/rating/{username}"}, method = RequestMethod.GET)
    public Result<List> userRatings(@PathVariable String username){
        return Result.success(ratingService.query(username));
    }
}