package com.oj.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.activation.FileDataSource;
import javax.activation.URLDataSource;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.net.URL;

@Slf4j
@Component
public class MailUtil {

    /**
     * Spring Boot 提供了一个发送邮件的简单抽象，使用的是下面这个接口，这里直接注入即可使用
     */
    private static JavaMailSender mailSender;

    @Autowired
    public void setRedisService (JavaMailSender mailSender){
        MailUtil.mailSender= mailSender;
    }

    /**
     * 配置文件中我的箱
     */
    private static String from;
    @Value("${spring.mail.properties.mail.from}")
    public void setFrom (String from){
        MailUtil.from= from;
    }

    /**
     * 简单文本邮件
     * @param to 收件人
     * @param subject 主题
     * @param content 内容
     */
    public void sendSimpleMail(String to, String subject, String content) {
        try {
            //创建SimpleMailMessage对象
            SimpleMailMessage message = new SimpleMailMessage();
            //邮件发送人
            message.setFrom(from);
            //邮件接收人
            message.setTo(to);
            //邮件主题
            message.setSubject(subject);
            //邮件内容
            message.setText(content);
            //发送邮件
            mailSender.send(message);
            log.info("Mail send ok.");
        }catch (Exception e){
            log.error(e.getMessage());
        }
    }

    /**
     * html邮件
     * @param to 收件人
     * @param subject 主题
     * @param content 内容
     */
    public void sendHtmlMail(String to, String subject, String content) {
        //获取MimeMessage对象
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper messageHelper;
        try {
            messageHelper = new MimeMessageHelper(message, true);
            //邮件发送人
            messageHelper.setFrom(from);
            //邮件接收人
            messageHelper.setTo(to);
            //邮件主题
            message.setSubject(subject);
            //邮件内容，html格式
            messageHelper.setText(content, true);
            //发送
            mailSender.send(message);
            //日志信息
            log.info("Html mail send ok.");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    /**
     * 带附件的邮件
     * @param to 收件人
     * @param subject 主题
     * @param content 内容
     * @param filePath 附件
     */
    public void sendAttachmentsMail(String to, String subject, String content, String filePath) {
        MimeMessage message = mailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(from);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(content, true);
            //本地或项目资源
            //FileSystemResource file = new FileSystemResource(new File(filePath));
            //FileDataSource fds = new FileDataSource(filePath);
            //远程资源
            URLDataSource uds=new URLDataSource(new URL(filePath));
            helper.addAttachment(uds.getName(), uds);
            mailSender.send(message);
            //日志信息
            log.info("Mail send ok, with attachments.");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    public static void main(String[] args) {
        MailUtil mailUtil = new MailUtil();
        mailUtil.sendHtmlMail("269574524@qq.com", "Test mail", "<b><a href='hapyoj.com'>happyoj.com</a></b>");
    }
}
