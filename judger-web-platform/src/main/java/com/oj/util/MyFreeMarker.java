package com.oj.util;

import java.io.*;
import java.util.Map;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.ResourceUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

public class MyFreeMarker {
    public static void generator(Template template, String desName, Map objMap) throws IOException, TemplateException{
        if (ResourceUtils.getURL("classpath:").getPath().startsWith("file:")) {
            desName = System.getProperty("user.dir") + "/templates/freemarker/" + desName;
        } else {
            desName = ResourceUtils.getURL("classpath:").getPath() + "/templates/freemarker/" + desName;
        }
        FileOutputStream file = new FileOutputStream(desName);
        Writer out = new OutputStreamWriter(new FileOutputStream(desName),"UTF-8");
        template.process(objMap, out);
    }
}
