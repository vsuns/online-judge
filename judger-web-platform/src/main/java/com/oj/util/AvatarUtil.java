package com.oj.util;

public class AvatarUtil {
    public static String avatarGenerate(String email) {
        String email_ = email.toLowerCase();
        String hash = HashUtil.sha256Hex(email_);
        return "https://gravatar.com/avatar/" + hash;
    }

    public static void main(String[] args) {
        System.out.println(avatarGenerate("xxxx@qq.com"));
    }
}
