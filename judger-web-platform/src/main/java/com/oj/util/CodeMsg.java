package com.oj.util;

import com.oj.service.impl.I18nService;

public class CodeMsg {
    private Integer code;
    private String msg;

    public static CodeMsg SUCCESS = new CodeMsg(200, "success");
    public static CodeMsg INNER_ERROR = new CodeMsg(100000, "Inner system fault, code %d");
    public static CodeMsg INNER_FAULT = new CodeMsg(100001, "Inner system fault");
    public static CodeMsg PARAM_INVALID = new CodeMsg(100002, "param is invalid");
    public static CodeMsg PARAM_NULL = new CodeMsg(100003, "param is null");
    public static CodeMsg INPUT_TITLE_NULL = new CodeMsg(100004, "输入的标题不能为空");
    public static CodeMsg INPUT_CONTENT_NULL = new CodeMsg(100005, "输入的内容不能为空");

    public static CodeMsg CONTEST_UNREG_ERR = new CodeMsg(200000, "Unregister contest failed");
    public static CodeMsg CONTEST_UNREG_ERR_STAT = new CodeMsg(200001, "The contest has already started");
    public static CodeMsg CONTEST_NEVER_REG = new CodeMsg(200002, "You has never been register this contest");
    public static CodeMsg PROBLEM_NO_SUCH = new CodeMsg(300000, "No such problem");

    public static CodeMsg USER_NOT_EXIST_OR_PSW_WRONG = new CodeMsg(400000, "Username not exist or password is wrong");
    public static CodeMsg USER_NOT_LOGIN = new CodeMsg(400001, "用户未登录");
    public static CodeMsg PASSWORD_ERROR = new CodeMsg(400002, "密码错误");
    public static CodeMsg CONFIRM_PASSWORD_ERROR = new CodeMsg(400003, "两次输入的密码不一致");
    public static CodeMsg PASSWORD_LEN_ERROR = new CodeMsg(400004, "密码长度需要大于等于6小于等于20");
    public static CodeMsg USERNAME_OR_EMAIL_NOTMATCH = new CodeMsg(400005, "用户名和邮箱不匹配");

    public static CodeMsg OPENSOURCE_IS_CLOSE = new CodeMsg(600000, "管理员已关闭源代码查看");
    public static CodeMsg USER_OPENSOURCE_IS_CLOSE = new CodeMsg(600001, "您所查看的代码设置了查看权限");
    public static CodeMsg VIEW_SOURCE_CONTEST_IS_RUNNING = new CodeMsg(600002, "比赛中的代码无法查看");
    public static CodeMsg SUBMIT_TOO_FREQUENTLY = new CodeMsg(600003, "代码提交过于频繁");

    public static CodeMsg TAG_IS_EXIST = new CodeMsg(700000, "标签已经存在");

    public static CodeMsg ADMIN_NO_ACCESS = new CodeMsg(800000, "no permission");

    private CodeMsg() {
    }

    private CodeMsg(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public CodeMsg locale() {
        String message = I18nService.getMessage(this.code.toString());
        return new CodeMsg(this.code, message);
    }

    public CodeMsg locale(Object... args) {
        String message = I18nService.getMessage(this.code.toString());
        return new CodeMsg(this.code, String.format(message, args));
    }

}
