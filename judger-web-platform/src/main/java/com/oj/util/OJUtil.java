package com.oj.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.oj.config.OJConfig;
import com.oj.entity.JudgeTest;
import com.oj.entity.Language;
import com.oj.entity.Problem;
import com.oj.service.ProblemService;
import com.oj.service.impl.I18nService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class OJUtil {
    @Autowired
    private static I18nService i18nService;

    @Autowired
    private static ProblemService problemService;

    public static List<Language> getAllLanguages(String szOjName, boolean withDisable) {
        List<Language> langs = new ArrayList<Language>();
        String jsonBuff = StreamHandler.read(OJConfig.getConfigFile());
        if (jsonBuff != null) {
            String langItem = "languages";
            if (szOjName.equals("HDU")) {
                langItem = "hdu_languages";
            } else if (szOjName.equals("CF")) {
                langItem = "cf_languages";
            }

            JSONObject jsonObject = JSONObject.parseObject(jsonBuff);
            JSONArray languages = jsonObject.getJSONArray(langItem);
            for (Object language : languages) {
                JSONObject languageObj = JSONObject.parseObject(language.toString());

                Integer local_language_id = 0;
                try {
                    local_language_id = Integer.parseInt(languageObj.getString("local_language_id"));
                } catch (Exception e) {
                    local_language_id = 0;
                }

                Language lang = new Language(Integer.parseInt(languageObj.getString("id")),
                        languageObj.getString("language_name"), local_language_id);

                Integer disable = new Integer(0);
                if (!withDisable) {
                    try {
                        disable = Integer.parseInt(languageObj.getString("disable"));
                    } catch (Exception e) {
                    }
                }
                if (disable == 0) {
                    langs.add(lang);
                }
            }
        }
        return langs;
    }

    public static List<Language> getSupportLanguages(String szOjName) {
        return getAllLanguages(szOjName, true);
    }

    public static List<Language> getActiveLanguages(String szOjName) {
        return getAllLanguages(szOjName, false);
    }

    public static String getLanguageName(String szOjName, Integer languageId) {
        List<Language> languages = getSupportLanguages(szOjName);
        if (languages == null) {
            return null;
        }
        for (Language language : languages) {
            if (language.getId().equals(languageId)) {
                return language.getLanguage_name();
            }
        }

        return null;
    }

    public static Integer getLocalLanguageId(String szOjName, Integer languageId) {
        List<Language> languages = getSupportLanguages(szOjName);
        if (languages == null) {
            return 0;
        }

        for (Language language : languages) {
            if (language.getId().equals(languageId)) {
                return language.getLocal_language_id();
            }
        }

        return 0;
    }

    public static List<Language> getSupportLanguages(Integer problemId) {
        String szOjName = "GUET";
        try {
            Problem problem = problemService.query(problemId);
            if (problem != null) {
                szOjName = problem.getOj_name();
            }
        } catch (Exception e) {
        }
        return getActiveLanguages(szOjName);
    }

    public static List<Language> getSupportLanguages(Integer contestId, String problemNum) {
        Problem problem = problemService.query(contestId);
        String szOjName = "GUET";
        if (problem != null) {
            szOjName = problem.getOj_name();
        }
        return getActiveLanguages(szOjName);
    }

    public static String getVerdictName(Integer verdictId, Integer testcase) {
        String verdictName = "";
        switch (verdictId) {
            case 3:
            case 5:
                verdictName = i18nService.getMessage("verdict"+verdictId);
                break;
            default:
                verdictName = i18nService.getMessage("verdict"+verdictId);
                if (testcase > 0) {
                    verdictName += " on test " + testcase;
                }
                break;
        }
        return verdictName;
    }

    public static String getConfigFile() {
        return OJConfig.getConfigFile();
    }

    public static String getCodeTemplateByLanguageId(Integer languageId) {
        try {
            String templatePath = OJConfig.getTemplatePath();
            return StreamHandler.readEx(templatePath + File.separator + languageId + ".txt");
        } catch (Exception e) {
            return "";
        }
    }
}
