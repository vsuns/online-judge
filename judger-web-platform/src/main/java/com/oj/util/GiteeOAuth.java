package com.oj.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.oj.config.OAuthConfig;
import com.oj.entity.OAuth;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class GiteeOAuth {
    public String getToken(String client_id, String client_secret, String code){
        try {
            String redirect_url = OAuthConfig.getGiteeRedrectUrl();
            String url = "https://gitee.com/oauth/token" ;
            url += "?grant_type=authorization_code";
            url += "&code="+code;
            url += "&client_id="+client_id;
            url += "&redirect_uri="+redirect_url;
            url += "&client_secret="+client_secret;
            log.info(url);
            JSONObject paramJson = new JSONObject();
            String json = HttpClientUtil.doPost(url, paramJson, null);
            Map maps = (Map)JSON.parse(json);
            return maps.get("access_token").toString();
        } catch (Exception e) {
            // TODO: handle exception
            log.error(e.getMessage());
            return null;
        }
    }
    public OAuth getUserInfo(String token){
        try {
            String url = "https://gitee.com/api/v5/user?access_token="+token;
            Map<String,String> headers = new HashMap<>(1,1);
            headers.put("accept", "application/json");
            String json = HttpClientUtil.doGet(url, headers);
            Map map = (Map)JSON.parse(json);
            OAuth oauth = new OAuth();
            oauth.setProvider("gitee");
            oauth.setLogin(map.get("login").toString());
            oauth.setNickname(map.get("name").toString());
            oauth.setAvatar_url(map.get("avatar_url").toString());
            return oauth;
        } catch (Exception e) {
            // TODO: handle exception
            log.error(e.getMessage());
            return null;
        }
    }
    public OAuth login(String client_id, String client_secret, String code){
        String token = getToken(client_id, client_secret, code);
        if (token == null) {
            return null;
        }
        return getUserInfo(token);
    }

    public static void main(String[] args){
        GiteeOAuth github = new GiteeOAuth();
        // https://github.com/login/oauth/authorize?client_id=1e4854a69a833e13c195
        //github.login("1e4854a69a833e13c195","0fc4a364989d19d9ad9e326e8d10f23e9e10d171 ","50f766c5a8249a1c0fe8");
        //main.test2();
    }
}
