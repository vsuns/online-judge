package com.oj.util;

import com.oj.service.impl.I18nService;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtil {
    @Autowired
    private static I18nService i18nService;
    public static String getWeek(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("E", Locale.ENGLISH);
        String week = sdf.format(date);
        return week;
    }
    public static Date StringToDate(String dateString, String formatString){
        try {
            DateFormat fm = new SimpleDateFormat(formatString);
            Date date = null;
            try {
                date = fm.parse(dateString);
            } catch (java.text.ParseException e) {
                // TODO Auto-generated catch block
                //e.printStackTrace();
                return null;
            } // Thu Jan 18 00:00:00 CST 2007
            return date;
        } catch (Exception e) {
            // TODO: handle exception
            return null;
        }
    }
    public static String DateToString(Date date, String formatString){
        try {
            DateFormat fm = new SimpleDateFormat(formatString);
            String str = new String();
            str = fm.format(date);
            return str;
        } catch (Exception e) {
            // TODO: handle exception
            return null;
        }
    }
    public static String penaltyString(int second) {

        int sec=second%60;
        second=second/60;
        int min=second%60;
        int hour=second=second/60;

        String nowTime=new String();

        if(hour<10) {
            if(hour==0) nowTime="00";
            nowTime="0"+hour;
        }else {nowTime=Integer.toString(hour);}

        if(min<10) {
            nowTime=nowTime+":0"+min;
        }else {nowTime=nowTime+":"+min;}

        if(sec<10) {
            nowTime=nowTime+":0"+sec;
        }else {nowTime=nowTime+":"+sec;}

        return nowTime;
    }
    public static String minuteToString(long minute) {
        long second = minute * 60;
        long sec = second%60;
        second = second/60;
        long min=second%60;
        long hour=second=second/60;

        String nowTime=new String();

        if(hour<10) {
            if(hour==0) nowTime="00";
            nowTime="0"+hour;
        }else {nowTime=Long.toString(hour);}

        if(min<10) {
            nowTime=nowTime+":0"+min;
        }else {nowTime=nowTime+":"+min;}

        return nowTime;
    }
    public static String secondToString(long second) {

        long sec=second%60;
        second=second/60;
        long min=second%60;
        long hour=second=second/60;

        String nowTime=new String();

        if(hour<10) {
            if(hour==0) nowTime="00";
            nowTime="0"+hour;
        }else {nowTime=Long.toString(hour);}

        if(min<10) {
            nowTime=nowTime+":0"+min;
        }else {nowTime=nowTime+":"+min;}

        if(sec<10) {
            nowTime=nowTime+":0"+sec;
        }else {nowTime=nowTime+":"+sec;}

        return nowTime;
    }

    public static String toFriendlyDate(Date time){
        if(time == null) return i18nService.getMessage("unknown");
        int ct = (int)((System.currentTimeMillis() - time.getTime())/1000);
        if (ct < 60)
            return ct + " " + i18nService.getMessage("seconds_before");
        if(ct < 3600)
            return Math.max(ct / 60,1) + " " + i18nService.getMessage("minutes_before");
        if(ct >= 3600 && ct < 86400)
            return ct / 3600 + " " + i18nService.getMessage("hours_before");
        if(ct >= 86400 && ct < 2592000){ //86400 * 30
            int day = ct / 86400 ;
            if(day>1){
                return day + " " + i18nService.getMessage("days_before");
            }
            return i18nService.getMessage("yesterday");
        }
        if(ct >= 2592000 && ct < 31104000) //86400 * 30
            return ct / 2592000 + " " + i18nService.getMessage("months_before");
        return ct / 31104000 + " " + i18nService.getMessage("years_before");
    }
    public static String getFriendlyTimeLeft(long timeLeft){
        if(timeLeft < 172800){
            return DateUtil.secondToString(timeLeft);
        };
        if(timeLeft >= 172800 && timeLeft < 2592000){ //86400 * 30
            long day = timeLeft / 86400 ;
            return day +i18nService.getMessage("days_after");
        }
        if(timeLeft >= 2592000 && timeLeft < 31104000) //86400 * 30
            return timeLeft / 2592000 +i18nService.getMessage("months_after");
        return timeLeft / 31104000 + i18nService.getMessage("years_after");
    }
}
