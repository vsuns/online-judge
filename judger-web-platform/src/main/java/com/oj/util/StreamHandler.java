package com.oj.util;

import com.oj.entity.OJFileReader;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class StreamHandler {
    public static String read(String fileName) {
        try {
            InputStreamReader isr = new InputStreamReader(new FileInputStream(fileName),"UTF-8");
            BufferedReader in = new BufferedReader(isr);
            StringBuffer sb = new StringBuffer();
            String line = new String();
            while ( (line = in.readLine()) != null ) {
                sb.append(line.trim());
                sb.append("\n");
            }
            in.close();
            return sb.toString();
        } catch (IOException ioe) {
            return null;
        }
    }

    public static String readEx(String fileName) {
        try {
            InputStreamReader isr = new InputStreamReader(new FileInputStream(fileName),"UTF-8");
            BufferedReader in = new BufferedReader(isr);
            StringBuffer sb = new StringBuffer();
            String line = new String();
            while ( (line = in.readLine()) != null ) {
                sb.append(line);
                sb.append("\n");
            }
            in.close();
            return sb.toString();
        } catch (IOException ioe) {
            return null;
        }
    }
    public static String readEx(File file) {
        return readEx(file.getPath());
    }

    public static boolean write(String fileName, String content) {
        try {
            File file = new File(fileName);
            try {
                file.createNewFile();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            byte[] strBytes = content.getBytes("UTF-8");
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(strBytes , 0, strBytes.length);
            fos.close();
            return true;
        } catch (IOException ioe) {
            ioe.printStackTrace();
            return false;
        }
    }

    public static OJFileReader readLineFile(String fileName, int skipLineNumber, int lineNumer, boolean reverse) throws IOException
    {
        try {
            String buf = "";
            OJFileReader re = new OJFileReader();
            InputStreamReader isr = new InputStreamReader(new FileInputStream(fileName),"UTF-8");
            BufferedReader reader = new BufferedReader(isr);
            // skip to the n line
            int m = 0;
            for (; m < skipLineNumber && reader.readLine() != null; ++ m);

            String line;
            int n = 0;
            for (; n < lineNumer && (line = reader.readLine()) != null; ++ n) {
                if (reverse) {
                    buf = line + "\r\n" + buf;
                } else {
                    buf += line + "\r\n";
                }
            }

            re.setReadLineNum(n);
            re.setNextLineNum(n + m);
            re.setContent(buf);
            return  re;
        } catch (IOException ioe) {
            log.error("---- read file by file catches a IOException, "+fileName+" not found ----");
            return null;
        }
    }

    public static List<File> getFileList(String pathName){
        File file = new File(pathName);
        //log.info("---- load " + file.getAbsolutePath() + " ----");
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            List<File> filelist =new ArrayList<File>();
            for(int i = 0; i < files.length; i++) {
                if(files[i].isFile()){
                    filelist.add(files[i]);
                }
            }
            return filelist;
        }
        return null;
    }
    public static List<File> getDirList(String pathName){
        File file = new File(pathName);
        //log.info("---- load " + file.getAbsolutePath() + " ----");
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            List<File> filelist =new ArrayList<File>();
            for(int i = 0; i < files.length; i++) {
                if(!files[i].isFile()){
                    filelist.add(files[i]);
                }
            }
            return filelist;
        }
        return null;
    }

    public static String[] getFilesNameList(String path) {
        /**
         * return a File[] of input-testcase's files by problem's id.
         */
        File file = new File(path);

        if (file.isDirectory())
            return file.list();
        return null;
    }

    public static boolean uploadFile(File file,String path,String fileName){
        try {
            File dir = new File(path);
            if(!dir.exists() ) {
                dir.mkdirs();
            }
            FileOutputStream fos = new FileOutputStream(path+"/" + fileName);
            FileInputStream fis = new FileInputStream(file);
            byte[] buffer = new byte[1024];
            int len = 0;
            while( (len = fis.read(buffer)) > 0 ) {
                fos.write(buffer, 0, len);
            }
            fos.close();
            fis.close();
            return true;
        } catch(Exception e) {
            return false;
        }
    }
    public static void main(String[] args) throws IOException {
        System.out.println("1001");
    }
}
