package com.oj.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.ProxyConfig;
import com.gargoylesoftware.htmlunit.SilentCssErrorHandler;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.oj.config.OAuthConfig;
import com.oj.entity.OAuth;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;

import java.util.HashMap;
import java.util.Map;

@Slf4j
public class GoogleOAuth {
    public String getToken(String client_id, String client_secret, String code){
        try {
            String redirect_url = OAuthConfig.getGoogleRedrectUrl();
            String url = "https://oauth2.googleapis.com/token" ;
            url += "?grant_type=authorization_code";
            url += "&code="+code;
            url += "&client_id="+client_id;
            url += "&redirect_uri="+redirect_url;
            url += "&client_secret="+client_secret;
            log.info(url);
            return null;
        } catch (Exception e) {
            // TODO: handle exception
            log.error(e.getMessage());
            return null;
        }
    }
    public OAuth getUserInfo(String token){
        try {
            String url = "https://gitee.com/api/v5/user?access_token="+token;
            Map<String,String> headers = new HashMap<>(1,1);
            headers.put("accept", "application/json");
            String json = HttpClientUtil.doGet(url, headers);
            Map map = (Map)JSON.parse(json);
            OAuth oauth = new OAuth();
            oauth.setProvider("gitee");
            oauth.setLogin(map.get("login").toString());
            oauth.setNickname(map.get("name").toString());
            oauth.setAvatar_url(map.get("avatar_url").toString());
            return oauth;
        } catch (Exception e) {
            // TODO: handle exception
            log.error(e.getMessage());
            return null;
        }
    }
    public OAuth login(String client_id, String client_secret, String code){
        String token = getToken(client_id, client_secret, code);
        if (token == null) {
            return null;
        }
        return getUserInfo(token);
    }

    public static void main(String[] args){
        GoogleOAuth github = new GoogleOAuth();
        // https://github.com/login/oauth/authorize?client_id=1e4854a69a833e13c195
        //github.login("1e4854a69a833e13c195","0fc4a364989d19d9ad9e326e8d10f23e9e10d171 ","50f766c5a8249a1c0fe8");
        //main.test2();
    }
}
