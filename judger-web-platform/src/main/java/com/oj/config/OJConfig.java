package com.oj.config;

import com.oj.entity.Language;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.Serializable;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

@Slf4j
@Component
public class OJConfig {
    private static String ojPath;

    private static String dataPath;

    private static String judgeLogPath;

    private static String configFile;

    private static String judgerIp;

    private static Integer judgerPort;

    private static String encode;

    private static String blackIpList;

    private static boolean opensource;

    private static String templatePath;


    private static String ip2regionPath;

    @Value("${config.ojPath}")
    public void setOjPath(String ojPath) {
        OJConfig.ojPath = Paths.get(ojPath).normalize().toString();
    }

    @Value("${config.dataPath}")
    public void setDataPath(String dataPath) {
        OJConfig.dataPath = dataPath;
    }

    @Value("${config.judgeLogPath}")
    public void setJudgeLogPath(String judgeLogPath) {
        OJConfig.judgeLogPath = judgeLogPath;
    }

    @Value("${config.configFile}")
    public void setConfigFile(String configFile) {
        OJConfig.configFile = configFile;
    }

    @Value("${config.judgerIp}")
    public void setJudgerIp(String judgerIp) {
        OJConfig.judgerIp = judgerIp;
    }

    @Value("${config.judgerPort}")
    public void setJudgerPort(Integer judgerPort) {
        OJConfig.judgerPort = judgerPort;
    }

    @Value("${config.encode}")
    public void setEncode(String encode) {
        OJConfig.encode = encode;
    }

    @Value("true")
    public void setEncode(boolean opensource) {
        OJConfig.opensource = opensource;
    }

    @Value("${config.templatePath}")
    public void setTemplatePath(String templatePath) {
        OJConfig.templatePath = templatePath;
    }

    @Value("${config.black-list}")
    public void setBlackIpList(String blackIpList) {
        OJConfig.blackIpList = blackIpList;
    }

    @Value("${config.ip2regionPath}")
    public void setIp2regionPath(String ip2regionPath) {
        OJConfig.ip2regionPath = Paths.get(ip2regionPath).normalize().toString();;
    }
    public static String getIp2regionPath() {
        return ip2regionPath;
    }

    public static String getOjPath() {
        return ojPath;
    }

    public static String getDataPath() {
        return dataPath;
    }

    public static String getJudgeLogPath() {
        return judgeLogPath;
    }

    public static String getConfigFile() {
        return configFile;
    }

    public static String getJudgerIp() {
        return judgerIp;
    }

    public static Integer getJudgerPort() {
        return judgerPort;
    }

    public static String getEncode() {
        return encode;
    }

    public static boolean getOpenSource() {
        return opensource;
    }

    public static String getBlackIpList() {
        return blackIpList;
    }

    public static String getTemplatePath() {
        return templatePath;
    }

    private static Set<String> blockIps;

    public static void initBlockIpList() {
        blockIps = new HashSet<String>();
        String[] splitAddress = blackIpList.split(",");
        for(String ip : splitAddress){
            blockIps.add(ip);
        }
    }
    public static void showBlockIpList() {
        Iterator<String> iter = blockIps.iterator();
        while(iter.hasNext()){
            log.info("Block IP: " + iter.next());
        }
    }
    public static boolean IsBlockIpList(String ip) {
        return blockIps.contains(ip);
    }
}
