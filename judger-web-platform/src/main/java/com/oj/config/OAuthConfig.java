package com.oj.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
public class OAuthConfig {
    private static String githubClientID;
    private static String githubClientSecret;
    private static String giteeClientID;
    private static String giteeClientSecret;
    private static String giteeRedrectUrl;
    private static String googleClientID;
    private static String googleClientSecret;
    private static String googleRedrectUrl;
    public static String getGithubClientID() {
        return githubClientID;
    }

    @Value("${oauth.githubClientID}")
    public void setGithubClientID(String githubClientID) {
        this.githubClientID = githubClientID;
    }

    public static String getGithubClientSecret() {
        return githubClientSecret;
    }

    @Value("${oauth.githubClientSecret}")
    public void setGithubClientSecret(String githubClientSecret) {
        this.githubClientSecret = githubClientSecret;
    }

    public static String getGiteeClientID() {
        return giteeClientID;
    }

    @Value("${oauth.giteeClientID}")
    public void setGiteeClientID(String giteeClientID) {
        this.giteeClientID = giteeClientID;
    }

    public static String getGiteeClientSecret() {
        return giteeClientSecret;
    }

    @Value("${oauth.giteeClientSecret}")
    public void setGiteeClientSecret(String giteeClientSecret) {
        this.giteeClientSecret = giteeClientSecret;
    }

    public static String getGiteeRedrectUrl() {
        return giteeRedrectUrl;
    }

    @Value("${oauth.giteeRedrectUrl}")
    public void setGiteeRedrectUrl(String giteeRedrectUrl) {
        this.giteeRedrectUrl = giteeRedrectUrl;
    }

    public static String getGoogleClientID() {
        return googleClientID;
    }
    @Value("${oauth.googleClientID}")
    public void setGoogleClientID(String googleClientID) {
        this.googleClientID = googleClientID;
    }

    public static String getGoogleClientSecret() {
        return googleClientSecret;
    }

    @Value("${oauth.googleClientSecret}")
    public void setGoogleClientSecret(String googleClientSecret) {
        this.googleClientSecret = googleClientSecret;
    }

    public static String getGoogleRedrectUrl() {
        return googleRedrectUrl;
    }

    @Value("${oauth.googleRedrectUrl}")
    public void setGoogleRedrectUrl(String googleRedrectUrl) {
        this.googleRedrectUrl = googleRedrectUrl;
    }
}
