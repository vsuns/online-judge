package com.oj.service;

import com.oj.entity.Rating;
import java.util.List;

public interface RatingService {
    List<Rating> query(String username);

    Rating query(Integer contestId, String username);

}
