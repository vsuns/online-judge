package com.oj.service;

import com.oj.entity.SolutionSource;

import java.util.List;

public interface SolutionSourceService {
    List<SolutionSource> query();

    SolutionSource query(Integer solutionId);
}
