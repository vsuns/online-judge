package com.oj.service;

public interface PrivilegeService {
    boolean admin(String username);
}
