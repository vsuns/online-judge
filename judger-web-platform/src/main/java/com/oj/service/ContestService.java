package com.oj.service;

import com.oj.entity.Contest;

import java.util.List;

public interface ContestService {
    List<Contest> query();

    Contest query(Integer contestId);

    boolean isContestEnded(Integer contestId);
}
