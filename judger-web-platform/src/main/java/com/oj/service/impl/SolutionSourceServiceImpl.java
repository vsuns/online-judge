package com.oj.service.impl;

import com.oj.entity.SolutionSource;
import com.oj.mapper.SolutionSourceMapper;
import com.oj.service.SolutionSourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SolutionSourceServiceImpl implements SolutionSourceService {
    @Autowired
    private SolutionSourceMapper solutionSourceMapper;

    @Override
    @Transactional
    public List<SolutionSource> query() {
        return solutionSourceMapper.queryList();
    }

    @Override
    @Transactional
    public SolutionSource query(Integer solutionId) {
        return solutionSourceMapper.query(solutionId);
    }
}
