package com.oj.service.impl;

import com.oj.mapper.PrivilegeMapper;
import com.oj.service.PrivilegeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class PrivilegeServiceImpl implements PrivilegeService {
    @Autowired
    private PrivilegeMapper privilegeMapper;

    @Override
    public boolean admin(String username){
        if (privilegeMapper.query(username, "HEAD") != null) {
            return true;
        }

        if (privilegeMapper.query(username, "ADMIN") != null) {
            return true;
        }

        return false;
    }
}
