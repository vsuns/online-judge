package com.oj.service.impl;

import com.oj.entity.Contest;
import com.oj.mapper.ContestMapper;
import com.oj.service.ContestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class ContestServiceImpl implements ContestService {
    @Autowired
    private ContestMapper contestMapper;

    @Override
    @Transactional
    public List<Contest> query() {
        return contestMapper.queryList();
    }

    @Override
    @Transactional
    public Contest query(Integer contestId) {
        return contestMapper.query(contestId);
    }

    public boolean isContestEnded(Integer contestId) {
        if (contestId == 0) {
            return false;
        }

        Contest contest = contestMapper.query(contestId);
        if (contest == null) {
            return false;
        }

        if (contest.getEnd_time().getTime() > new Date().getTime()) {
            return false;
        }

        return true;
    }
}
