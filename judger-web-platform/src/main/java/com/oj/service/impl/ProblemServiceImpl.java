package com.oj.service.impl;

import com.oj.entity.Problem;
import com.oj.mapper.ProblemMapper;
import com.oj.service.ProblemService;
import com.oj.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ProblemServiceImpl implements ProblemService {
    @Autowired
    private ProblemMapper problemMapper;
    @Override
    @Transactional
    public List<Problem> search(String q, Integer limit, Integer offset) {
        Map param = new HashMap();
        param.put("defunct", 'N');
        param.put("q", q);
        param.put("limit", limit);
        param.put("offset", offset);
        return problemMapper.search(param);
    }
    @Override
    @Transactional
    public List<Problem> searchAdmin(String q, Integer limit, Integer offset) {
        Map param = new HashMap();
        param.put("q", q);
        param.put("limit", limit);
        param.put("offset", offset);
        return problemMapper.search(param);
    }
    @Override
    @Transactional
    public List<Problem> query() {
        return problemMapper.query(null);
    }

    @Override
    @Transactional
    public Problem query(Integer problemId) {
        List<Problem> list = problemMapper.query(problemId);
        if (list == null || list.size() == 0) {
            return null;
        }

        return list.get(0);
    }

    @Override
    @Transactional
    public Problem queryAdmin(Integer problemId) {
        return problemMapper.queryAdmin(problemId);
    }
}
