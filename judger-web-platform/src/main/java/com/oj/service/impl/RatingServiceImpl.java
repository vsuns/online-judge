package com.oj.service.impl;

import com.oj.entity.Contest;
import com.oj.entity.Rating;
import com.oj.mapper.ContestMapper;
import com.oj.mapper.RatingMapper;
import com.oj.service.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class RatingServiceImpl implements RatingService {
    @Autowired
    private I18nService i18nService;

    @Autowired
    private RatingMapper ratingMapper;

    @Autowired
    private ContestMapper contestMapper;

    @Override
    @Transactional
    public List<Rating> query(String username) {
        List<Rating> ratings = ratingMapper.queryList(username);
        for (Rating rating:ratings) {
            Contest contest = contestMapper.query(rating.getContest_id());
            if (contest != null) {
                rating.setRate(getRateByRating(rating.getRating()));
                rating.setRating_title(i18nService.getMessage("user_rate" + rating.getRate()));
                rating.setContest_name(contest.getTitle());
            }
        }
        return ratings;
    }

    @Override
    @Transactional
    public Rating query(Integer contestId, String username) {
        Rating rating = ratingMapper.query(contestId, username);
        if (rating != null) {
            Contest contest = contestMapper.query(rating.getContest_id());
            if (contest != null) {
                rating.setRate(getRateByRating(rating.getRating()));
                rating.setRating_title(i18nService.getMessage("user_rate" + rating.getRate()));
                rating.setContest_name(contest.getTitle());
            }
        }
        return rating;
    }

    public Integer getRateByRating(Integer rating) {
        if (rating < 1200 && rating > 0) {
            return 1;
        }
        if (rating < 1400) {
            return 2;
        }
        if (rating < 1600) {
            return 3;
        }
        if (rating < 1750) {
            return 4;
        }
        if (rating < 1900) {
            return 5;
        }
        if (rating < 2050) {
            return 6;
        }
        if (rating < 2200) {
            return 7;
        }
        if (rating < 2600) {
            return 8;
        }
        if (rating < 3000) {
            return 9;
        }
        if (rating >= 3000) {
            return 10;
        }
        return 0;
    }
}
