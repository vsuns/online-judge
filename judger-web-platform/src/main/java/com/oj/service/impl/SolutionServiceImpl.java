package com.oj.service.impl;

import com.oj.entity.Solution;
import com.oj.mapper.SolutionMapper;
import com.oj.service.SolutionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SolutionServiceImpl implements SolutionService {
    @Autowired
    private SolutionMapper solutionMapper;

    @Override
    @Transactional
    public List<Solution> query() {
        return solutionMapper.queryList();
    }

    @Override
    @Transactional
    public Solution query(Integer solutionId) {
        return solutionMapper.query(solutionId);
    }
}
