package com.oj.service;

import com.oj.entity.Problem;

import java.util.List;
import java.util.Map;

public interface ProblemService {
    List<Problem> search(String q, Integer limit, Integer offset);
    List<Problem> searchAdmin(String q, Integer limit, Integer offset);

    List<Problem> query();

    Problem query(Integer problemId);

    Problem queryAdmin(Integer problemId);
}
