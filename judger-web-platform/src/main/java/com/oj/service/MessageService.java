package com.oj.service;

import com.oj.entity.Message;

import java.util.List;

public interface MessageService {
    void topTopicsGenerator();

    List<Message> search(String q, Integer limit, Integer offset);
}
