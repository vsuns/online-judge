package com.oj.service;

import com.oj.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface UserService {
    String getTokenCookieName();

    String getUsernameCookieName();

    List<User> search(String q, Integer limit, Integer offset);

    List<User> query();

    List<User> topUsers();

    Integer getUserRank(User user);

    User query(String username);

    boolean update(User user);

    boolean login(HttpServletRequest request, HttpServletResponse response, String username, String password);

    void logout(HttpServletRequest request, HttpServletResponse response);

    String register(HttpServletRequest request, HttpServletResponse response, User user);

    User getUserBySession(HttpServletRequest request);
}
