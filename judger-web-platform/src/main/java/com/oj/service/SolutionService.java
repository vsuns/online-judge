package com.oj.service;

import com.oj.entity.Solution;

import java.util.List;

public interface SolutionService {
    List<Solution> query();

    Solution query(Integer solutionId);
}
