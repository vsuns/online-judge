package com.oj.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Directory implements Serializable {
    private String type;
    private String root;
    private String parent;
    private String current;
    private List<OJFile> files;
}
