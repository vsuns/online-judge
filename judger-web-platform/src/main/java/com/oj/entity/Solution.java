package com.oj.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
public class Solution implements Serializable {
    private Integer solution_id;
    private Integer problem_id;
    private String username;
    private Date submit_date;
    private Integer memory;
    private Integer time;
    private Integer code_length;
    private Integer language;
    private String language_name;
    private Integer verdict;
    private Integer contest_id;
    private Integer testcase;
    private Integer failcase;
    private String json_result;

    private Integer timeout;
    private Integer opensource;
    private String status_description;
    private String friendlySubmitDate;
    private String problemNum;
    private String timeSinceContestStart;
    private String judgeLog;

    private Problem problem;
    private ContestProblem contestProblem;
    private User user;
    private SolutionSource solutionSource;
    private Contest contest;

    public Solution() {
        this.memory = 0;
        this.time = 0;
        this.contest_id = 0;
        this.testcase = 0;
        this.verdict = 1;
        this.timeout = 0;
        this.opensource = 0;
    }
}
