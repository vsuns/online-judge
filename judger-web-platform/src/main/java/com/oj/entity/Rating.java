package com.oj.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Rating implements Serializable {
    private Integer id;
    private String username;
    private Integer contest_id;
    private Integer rank;
    private Integer delta;
    private Integer rating;
    private Date rating_date;

    private Integer rate;
    private String contest_name;
    private String rating_title;
    private String nickname;
}
