package com.oj.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Attend implements Serializable {
    private Integer id;
    private Integer contest_id;
    private String username;
    private Integer solved;
    private Integer score;
    private Integer penalty;
    private String defunct;

    private Integer A_time;
    private Integer A_wrongsubmits;
    private Integer B_time;
    private Integer B_wrongsubmits;
    private Integer C_time;
    private Integer C_wrongsubmits;
    private Integer D_time;
    private Integer D_wrongsubmits;
    private Integer E_time;
    private Integer E_wrongsubmits;
    private Integer F_time;
    private Integer F_wrongsubmits;
    private Integer G_time;
    private Integer G_wrongsubmits;
    private Integer H_time;
    private Integer H_wrongsubmits;
    private Integer I_time;
    private Integer I_wrongsubmits;
    private Integer J_time;
    private Integer J_wrongsubmits;
    private Integer K_time;
    private Integer K_wrongsubmits;
    private Integer L_time;
    private Integer L_wrongsubmits;
    private Integer M_time;
    private Integer M_wrongsubmits;
    private Integer N_time;
    private Integer N_wrongsubmits;
    private Integer O_time;
    private Integer O_wrongsubmits;
    private Integer P_time;
    private Integer P_wrongsubmits;
    private Integer Q_time;
    private Integer Q_wrongsubmits;
    private Integer R_time;
    private Integer R_wrongsubmits;
    private Integer S_time;
    private Integer S_wrongsubmits;
    private Integer T_time;
    private Integer T_wrongsubmits;
    private Integer U_time;
    private Integer U_wrongsubmits;
    private Integer V_time;
    private Integer V_wrongsubmits;
    private Integer W_time;
    private Integer W_wrongsubmits;
    private Integer X_time;
    private Integer X_wrongsubmits;
    private Integer Y_time;
    private Integer Y_wrongsubmits;
    private Integer Z_time;
    private Integer Z_wrongsubmits;

    private User user;
    private Rating rating;
    private Integer rank;
    private String penaltyFriendly;
    private List<String> acTimeList;
    private List<Integer> acTimeIntList;
    private List<Integer> wrongSubmits;

    public Integer getACtime(Integer i) {
        switch(i)
        {
            case 1:
                return A_time;
            case 2:
                return B_time;
            case 3:
                return C_time;
            case 4:
                return D_time;
            case 5:
                return E_time;
            case 6:
                return F_time;
            case 7:
                return G_time;
            case 8:
                return H_time;
            case 9:
                return I_time;
            case 10:
                return J_time;
            case 11:
                return K_time;
            case 12:
                return L_time;
            case 13:
                return M_time;
            case 14:
                return N_time;
            case 15:
                return O_time;
            case 16:
                return P_time;
            case 17:
                return Q_time;
            case 18:
                return R_time;
            case 19:
                return S_time;
            case 20:
                return T_time;
            case 21:
                return U_time;
            case 22:
                return V_time;
            case 23:
                return W_time;
            case 24:
                return X_time;
            case 25:
                return Y_time;
            case 26:
                return Z_time;
        }
        return 0;
    }
    public Integer getWrongSubmits(Integer i) {
        switch(i)
        {
            case 1:
                return A_wrongsubmits;
            case 2:
                return B_wrongsubmits;
            case 3:
                return C_wrongsubmits;
            case 4:
                return D_wrongsubmits;
            case 5:
                return E_wrongsubmits;
            case 6:
                return F_wrongsubmits;
            case 7:
                return G_wrongsubmits;
            case 8:
                return H_wrongsubmits;
            case 9:
                return I_wrongsubmits;
            case 10:
                return J_wrongsubmits;
            case 11:
                return K_wrongsubmits;
            case 12:
                return L_wrongsubmits;
            case 13:
                return M_wrongsubmits;
            case 14:
                return N_wrongsubmits;
            case 15:
                return O_wrongsubmits;
            case 16:
                return P_wrongsubmits;
            case 17:
                return Q_wrongsubmits;
            case 18:
                return R_wrongsubmits;
            case 19:
                return S_wrongsubmits;
            case 20:
                return T_wrongsubmits;
            case 21:
                return U_wrongsubmits;
            case 22:
                return V_wrongsubmits;
            case 23:
                return W_wrongsubmits;
            case 24:
                return X_wrongsubmits;
            case 25:
                return Y_wrongsubmits;
            case 26:
                return Z_wrongsubmits;
        }
        return 0;
    }
}
