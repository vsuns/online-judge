package com.oj.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OJFile implements Serializable {
    private String name;

    private long length;

    private int isDirectory;

    private Date lastModified;

}
