package com.oj.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
public class Problem implements Serializable {
    private Integer problem_id;
    private String title;
    private String description;
    private String input;
    private String output;
    private String sample_input;
    private String sample_output;
    private Date create_date;
    private Integer submit;
    private Integer accepted;
    private Integer submit_user;
    private Integer solved;
    private float ratio;
    private Integer memory_limit;
    private Integer time_limit;
    private String author;
    private String source;
    private float difficulty;
    private String hint;
    private Integer spj;
    private String defunct;
    private String tag;
    private Integer contest_id;
    private Integer editor_mode; /* 0:keeditor, 1:markdown */

    private String oj_name;
    private String oj_pid;
    private Integer isvirtual;
    private List<Tags> tags;
    private Integer status; /* 0: not try, 1: solved, 2: tried */

    public Problem() {
        this.problem_id = 0;
        this.time_limit = 1000;
        this.memory_limit = 65535;
        this.submit = 0;
        this.accepted = 0;
        this.submit_user = 0;
        this.solved = 0;
        this.ratio = 0;
        this.difficulty = 0;
        this.spj = 0;
        this.create_date=new Date();
        this.oj_name="GUET";
        this.oj_pid = "0";
        this.isvirtual = 0 ;
        this.defunct = "N";
        this.editor_mode = 1;
    }
}
