package com.oj.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Message implements Serializable {
    private Integer message_id;
    private Integer problem_id;
    private Integer contest_id;
    private Integer module_id;
    private Integer parent_id;
    private Integer root_id;
    private Integer votes;
    private Integer mode;
    private Integer views;
    private String kviews;
    private Integer orderNum;
    private String title;
    private String content;
    private String defunct;
    private Date in_date;
    private String create_user;

    private String content_abstract;
    private String friendly_Date;
    private User user;
    private User lastRplUser;
    private Date lastReplyDate;
    private String lastReplyFriendlyDate;
    private Integer lastReplyId;
    private Integer comments;
    private Integer commentDepth;
    private String tag;
    private List<Tags> tags;
}
