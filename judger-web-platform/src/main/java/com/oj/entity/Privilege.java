package com.oj.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Privilege implements Serializable {
    private Integer id;
    private String username;
    private String rightstr;

    private User user;
}
