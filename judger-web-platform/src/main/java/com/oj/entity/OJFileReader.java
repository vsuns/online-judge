package com.oj.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OJFileReader {
    private String type;
    private String fileName;
    private String content;
    private Integer fromLineNum;
    private Integer readLineNum;
    private Integer nextLineNum;
}
