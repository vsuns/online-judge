package com.oj.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
public class Tags implements Serializable {

    private Integer tag_id;
    private String name;
    private String description;
    private String create_user;
    private Date indate;
    private String defunct;
    private Integer count;

    private String createFriendlyDate;
    private Integer problem_count;
    private Integer topic_count;

    public Tags(){
        this.defunct="N";
    }
}
