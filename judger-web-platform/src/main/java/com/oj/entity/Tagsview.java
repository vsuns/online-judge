package com.oj.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Tagsview implements Serializable {
    private Integer id;
    private Integer tag_id;
    private Integer problem_id;
    private Integer message_id;
}
