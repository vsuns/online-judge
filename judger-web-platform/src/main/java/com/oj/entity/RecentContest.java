package com.oj.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RecentContest implements Serializable {
    private String oj;
    private String link;
    private String name;
    private Date start_date;
    private String start_time;
    private Date end_date;
    private String end_time;
    private String week;
    private String access;
}
