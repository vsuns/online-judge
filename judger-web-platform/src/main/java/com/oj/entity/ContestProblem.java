package com.oj.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
public class ContestProblem implements Serializable {
    public interface updateValid {};

    private Integer id;
    private Integer problem_id;
    private Integer contest_id;
    private String  title;
    private String  num;
    private Integer submit;
    private Integer accepted;
    private Integer submit_user;
    private Integer solved;
    private Integer point;
    private String defunct;

    public ContestProblem() {
        this.submit = 0;
        this.accepted = 0;
        this.submit_user = 0;
        this.solved = 0;
        this.point = 1;
        this.defunct = "N";
    }
}
