package com.oj.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JudgeTest implements Serializable {
    private String  session_id;
    private String  code;
    private String  input;
    private String  output;
    private String  verdict;
    private String  compile_error;
    private String  time;
    private String  memory;
    private String  problem_id;
    private String  language_id;
}
