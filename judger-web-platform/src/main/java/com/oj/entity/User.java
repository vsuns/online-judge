package com.oj.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Email;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {
    public interface registerValid {};
    public interface updateValid {};
    public interface loginValid {};
    public interface passRecoveryValid {};
    private Integer id;

    @Pattern(groups = {registerValid.class, passRecoveryValid.class}, regexp = "^\\s*|[0-9A-Za-z_]*$", message = "{validation.user.username.format}")
    @NotBlank(groups = {registerValid.class, passRecoveryValid.class}, message = "{validation.user.username.notblank}")
    @Size(groups = {registerValid.class}, min = 6, max = 20, message = "{validation.user.username.size}")
    private String username;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @NotBlank(groups = {registerValid.class}, message = "{validation.user.password.notblank}")
    @Size(groups = {registerValid.class}, min = 6, max = 20, message = "{validation.user.password.size}")
    private String password;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String old_password;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String repeat_password;

    private String nickname;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String token;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @NotBlank(groups = {registerValid.class, updateValid.class, passRecoveryValid.class}, message = "{validation.user.email.notblank}")
    @Email(groups = {registerValid.class, updateValid.class, passRecoveryValid.class}, message = "{validation.user.email.format}")
    private String email;

    private String school;
    private String motto;
    private String opensource;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Date birthday;
    private Date regdate;
    private Date lastlogin;
    private Date lastaccesstime;
    private Integer language;
    private Integer solved;
    private Integer submit;
    private Integer rating;
    private Integer rating_max;
    private String avatar;
    private Integer rate;
    private Integer rate_max;
    private String languageName;
    private String ip;
    private String city;
    private String lastAccessTimeFriendly;
    private String registerDateFriendly;
    private Integer online;
    private String onlineState;
    private Integer rank;
}
