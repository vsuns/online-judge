package com.oj.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
public class Mail implements Serializable {
    private Integer mail_id;
    private String title;
    private String content;
    private String from_user;
    private String to_user;
    private Integer isnew;
    private Integer reply;
    private Date create_date;
    private String defunct;

    private String friendlyDate;
    private User sendUser;
    private User recvUser;

    public Mail() {
        this.isnew=1;
        this.create_date=new Date();
        this.defunct="N";
    }
}
