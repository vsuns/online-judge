package com.oj.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OAuth implements Serializable {
    private Integer id;
    private String username;
    private String provider;
    private String login;
    private String nickname;
    private String avatar_url;
    private Date create_date;
}
