package com.oj.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OJTestCaseFile implements Serializable {
    private Integer problemId;
    private String inFileName;
    private String outFileName;
    private String inputString;
    private String outputString;
}
