package com.oj.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
public class Contest implements Serializable {
    public interface updateValid {};

    private Integer contest_id;
    private String title;
    private String description;
    private Date start_time;
    private Date end_time;
    private Date start_reg;
    private Date end_reg;
    private Date create_time;
    private String password;
    private String create_user;
    private String defunct;
    private Integer type;

    private String leftTime;
    private String regleftTime;
    private String during;
    private Integer registrants;
    private String status;
    private String regStatus;
    private String isRegister;
    private String friendlyLeftTime;
    private String friendlyRegleftTime;

    private List<Attend> attendList;

    private User user;

    private Integer shour;
    private Integer sminute;

    private Integer ehour;
    private Integer eminute;

    private Integer shour_reg;
    private Integer sminute_reg;

    private Integer ehour_reg;
    private Integer eminute_reg;

    public Contest() {
        this.contest_id = 0;
        this.defunct = "N";
        this.type = 0;
        this.start_time=new Date();
        this.end_time=new Date();
        this.start_reg=new Date();
        this.end_reg=new Date();
        this.shour = 0;
        this.sminute = 0;
        this.ehour = 0;
        this.eminute = 0;
        this.shour_reg = 0;
        this.sminute_reg = 0;
        this.ehour_reg = 0;
        this.eminute_reg = 0;
    }
}
