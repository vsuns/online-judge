package com.oj.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@AllArgsConstructor
public class OnlineUser {
    private String username;
    private Date loginDate;
    private Date lastAccessTime;
    private Integer level;
    private String levelTitle;
    private Integer online;

    public OnlineUser() {
        super();
        SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.loginDate = Timestamp.valueOf(simpleDate.format(new Date()));
        this.lastAccessTime = Timestamp.valueOf(simpleDate.format(new Date()));
        this.online = 1;
    }
}
