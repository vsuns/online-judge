package com.oj.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class Submit implements Serializable {
    public interface updateValid {};

    private Integer mode;
    private String problemId;
    private Integer contestId;
    private Integer language;
    private String source;
    private String input;
    private Integer solutionId;
}
