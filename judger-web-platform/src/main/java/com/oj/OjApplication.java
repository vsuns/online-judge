package com.oj;

import com.oj.config.OJConfig;
import com.oj.service.impl.I18nService;
import com.oj.util.OJUtil;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.annotation.Resource;
import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Locale;
import java.util.TimeZone;

@Slf4j
@MapperScan("com.oj.mapper")
@EnableScheduling  //扫描定时器
@SpringBootApplication
public class OjApplication {

    public static void main(String[] args) throws UnknownHostException {
        TimeZone.setDefault(TimeZone.getTimeZone("Asia/Shanghai"));
        ConfigurableApplicationContext application = SpringApplication.run(OjApplication.class, args);

        Environment env = application.getEnvironment();
        log.info("config.judgerIp=" + env.getProperty("config.judgerIp"));
        log.info("config.judgerPort=" + env.getProperty("config.judgerPort"));
        log.info("config.ojPath=" + env.getProperty("config.ojPath"));
        log.info("config.dataPath=" + env.getProperty("config.dataPath"));
        log.info("config.judgeLogPath=" + env.getProperty("config.judgeLogPath"));
        log.info("config.templatePath=" + env.getProperty("config.templatePath"));
        log.info("config.configFile=" + env.getProperty("config.configFile"));
        log.info("config.opensource=" + env.getProperty("config.opensource"));
        log.info("oauth.githubClientID=" + env.getProperty("oauth.githubClientID"));
        log.info("oauth.githubClientSecret=" + env.getProperty("oauth.githubClientSecret"));
        log.info("oauth.giteeClientID=" + env.getProperty("oauth.giteeClientID"));
        log.info("oauth.giteeClientSecret=" + env.getProperty("oauth.giteeClientSecret"));
        log.info("oauth.giteeRedrectUrl=" + env.getProperty("oauth.giteeRedrectUrl"));

        OJConfig.initBlockIpList();
        OJConfig.showBlockIpList();
    }
}
