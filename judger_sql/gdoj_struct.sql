-- MySQL dump 10.13  Distrib 8.0.32, for Linux (x86_64)
--
-- Host: localhost    Database: gdoj
-- ------------------------------------------------------
-- Server version	8.0.32-0ubuntu0.20.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `gdoj`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `gdoj` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `gdoj`;

--
-- Table structure for table `attend`
--

DROP TABLE IF EXISTS `attend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `attend` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) DEFAULT NULL,
  `contest_id` int DEFAULT '0',
  `defunct` char(1) DEFAULT NULL,
  `solved` int DEFAULT '0',
  `score` int DEFAULT '0',
  `penalty` int DEFAULT '0',
  `A_time` int DEFAULT '0',
  `A_wrongsubmits` int DEFAULT '0',
  `B_time` int DEFAULT '0',
  `B_wrongsubmits` int DEFAULT '0',
  `C_time` int DEFAULT '0',
  `C_wrongsubmits` int DEFAULT '0',
  `D_time` int DEFAULT '0',
  `D_wrongsubmits` int DEFAULT '0',
  `E_time` int DEFAULT '0',
  `E_wrongsubmits` int DEFAULT '0',
  `F_time` int DEFAULT '0',
  `F_wrongsubmits` int DEFAULT '0',
  `G_time` int DEFAULT '0',
  `G_wrongsubmits` int DEFAULT '0',
  `H_time` int DEFAULT '0',
  `H_wrongsubmits` int DEFAULT '0',
  `I_time` int DEFAULT '0',
  `I_wrongsubmits` int DEFAULT '0',
  `J_time` int DEFAULT '0',
  `J_wrongsubmits` int DEFAULT '0',
  `K_time` int DEFAULT '0',
  `K_wrongsubmits` int DEFAULT '0',
  `L_time` int DEFAULT '0',
  `L_wrongsubmits` int DEFAULT '0',
  `M_time` int DEFAULT '0',
  `M_wrongsubmits` int DEFAULT '0',
  `N_time` int DEFAULT '0',
  `N_wrongsubmits` int DEFAULT '0',
  `O_time` int DEFAULT '0',
  `O_wrongsubmits` int DEFAULT '0',
  `P_time` int DEFAULT '0',
  `P_wrongsubmits` int DEFAULT '0',
  `Q_time` int DEFAULT '0',
  `Q_wrongsubmits` int DEFAULT '0',
  `R_time` int DEFAULT '0',
  `R_wrongsubmits` int DEFAULT '0',
  `S_time` int DEFAULT '0',
  `S_wrongsubmits` int DEFAULT '0',
  `T_time` int DEFAULT '0',
  `T_wrongsubmits` int DEFAULT '0',
  `U_time` int DEFAULT '0',
  `U_wrongsubmits` int DEFAULT '0',
  `V_time` int DEFAULT '0',
  `V_wrongsubmits` int DEFAULT '0',
  `W_time` int DEFAULT '0',
  `W_wrongsubmits` int DEFAULT '0',
  `X_time` int DEFAULT '0',
  `X_wrongsubmits` int DEFAULT '0',
  `Y_time` int DEFAULT '0',
  `Y_wrongsubmits` int DEFAULT '0',
  `Z_time` int DEFAULT '0',
  `Z_wrongsubmits` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=gbk;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `compile_info`
--

DROP TABLE IF EXISTS `compile_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `compile_info` (
  `solution_id` int NOT NULL DEFAULT '0',
  `error` text CHARACTER SET gbk COLLATE gbk_chinese_ci,
  PRIMARY KEY (`solution_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contest`
--

DROP TABLE IF EXISTS `contest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contest` (
  `contest_id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `description` text,
  `password` varchar(30) DEFAULT NULL,
  `create_user` varchar(30) DEFAULT NULL,
  `defunct` char(1) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `start_reg` datetime DEFAULT NULL,
  `end_reg` datetime DEFAULT NULL,
  `type` int DEFAULT '0',
  PRIMARY KEY (`contest_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=gbk;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contest_problem`
--

DROP TABLE IF EXISTS `contest_problem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contest_problem` (
  `id` int NOT NULL AUTO_INCREMENT,
  `problem_id` int NOT NULL,
  `contest_id` int NOT NULL,
  `title` varchar(255) NOT NULL,
  `num` varchar(10) NOT NULL,
  `submit` int DEFAULT NULL,
  `accepted` int DEFAULT NULL,
  `solved` int DEFAULT NULL,
  `submit_user` int DEFAULT NULL,
  `defunct` char(1) DEFAULT NULL,
  `point` int DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=gbk;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mail`
--

DROP TABLE IF EXISTS `mail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mail` (
  `mail_id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text,
  `from_user` varchar(30) DEFAULT NULL,
  `to_user` varchar(30) DEFAULT NULL,
  `isnew` int DEFAULT NULL,
  `reply` int DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `defunct` char(1) DEFAULT NULL,
  PRIMARY KEY (`mail_id`,`title`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=gbk;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `message` (
  `message_id` int NOT NULL AUTO_INCREMENT,
  `problem_id` int DEFAULT '0',
  `contest_id` int DEFAULT '0',
  `module_id` int NOT NULL DEFAULT '0',
  `parent_id` int DEFAULT '0',
  `root_id` int DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `orderNum` int DEFAULT '0',
  `in_date` datetime DEFAULT NULL,
  `create_user` varchar(20) NOT NULL,
  `defunct` char(1) DEFAULT NULL,
  `votes` int DEFAULT '0',
  `views` int DEFAULT '0',
  `mode` int DEFAULT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=gbk;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `news` (
  `news_id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `date` datetime DEFAULT NULL,
  `create_user` varchar(255) DEFAULT NULL,
  `orderNum` int DEFAULT NULL,
  `defunct` char(1) DEFAULT NULL,
  PRIMARY KEY (`news_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=gbk;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `oauth`
--

DROP TABLE IF EXISTS `oauth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `provider` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `login` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `nickname` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `avatar_url` varchar(256) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `privilege`
--

DROP TABLE IF EXISTS `privilege`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `privilege` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `rightstr` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=gbk;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `problem`
--

DROP TABLE IF EXISTS `problem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `problem` (
  `problem_id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text,
  `input` text,
  `output` text,
  `sample_input` text,
  `sample_output` text,
  `spj` int DEFAULT NULL,
  `hint` text,
  `source` varchar(255) DEFAULT NULL,
  `time_limit` int DEFAULT NULL,
  `memory_limit` int DEFAULT NULL,
  `defunct` char(1) DEFAULT NULL,
  `accepted` int DEFAULT NULL,
  `solved` int DEFAULT NULL,
  `submit` int DEFAULT NULL,
  `submit_user` int DEFAULT NULL,
  `author` varchar(30) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `difficulty` float DEFAULT '0',
  `ratio` float DEFAULT '0',
  `tag` varchar(255) DEFAULT NULL,
  `contest_id` int DEFAULT NULL,
  `oj_name` varchar(255) DEFAULT NULL,
  `oj_pid` varchar(32) DEFAULT NULL,
  `isvirtual` int(1) unsigned zerofill DEFAULT NULL,
  `editor_mode` int DEFAULT NULL,
  PRIMARY KEY (`problem_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1001 DEFAULT CHARSET=gbk;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rating`
--

DROP TABLE IF EXISTS `rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rating` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `contest_id` int NOT NULL,
  `rank` int DEFAULT NULL,
  `delta` int DEFAULT NULL,
  `rating` int DEFAULT NULL,
  `rating_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `solution`
--

DROP TABLE IF EXISTS `solution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `solution` (
  `solution_id` int NOT NULL AUTO_INCREMENT,
  `problem_id` int NOT NULL,
  `username` varchar(30) NOT NULL,
  `submit_date` datetime DEFAULT NULL,
  `memory` int DEFAULT NULL,
  `time` int DEFAULT NULL,
  `code_length` int DEFAULT NULL,
  `language` tinyint DEFAULT NULL,
  `language_name` text,
  `verdict` tinyint DEFAULT NULL,
  `contest_id` int DEFAULT NULL,
  `testcase` int DEFAULT NULL,
  `failcase` int DEFAULT NULL,
  `json_result` text,
  PRIMARY KEY (`solution_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=gbk;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `solution_source`
--

DROP TABLE IF EXISTS `solution_source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `solution_source` (
  `solution_id` int NOT NULL,
  `source` text,
  PRIMARY KEY (`solution_id`)
) ENGINE=InnoDB DEFAULT CHARSET=gbk;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tags` (
  `count` int DEFAULT '0',
  `description` text,
  `tag_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `indate` datetime DEFAULT NULL,
  `create_user` varchar(20) DEFAULT NULL,
  `defunct` char(1) DEFAULT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=gbk;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tagsview`
--

DROP TABLE IF EXISTS `tagsview`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tagsview` (
  `tag_id` int NOT NULL,
  `problem_id` int DEFAULT NULL,
  `message_id` int DEFAULT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `tag_id` (`tag_id`),
  KEY `problem_id` (`problem_id`),
  KEY `message_id` (`message_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=gbk;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nickname` varchar(20) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `school` varchar(100) DEFAULT NULL,
  `motto` varchar(255) DEFAULT NULL,
  `defunct` char(1) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `regdate` datetime DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `language` int DEFAULT NULL,
  `opensource` char(1) DEFAULT NULL,
  `solved` int DEFAULT '0',
  `submit` int DEFAULT '0',
  `rating` int DEFAULT '0',
  `lastlogin` datetime DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `lastaccesstime` datetime DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `rate` int DEFAULT '0',
  PRIMARY KEY (`id`,`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=gbk;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vote`
--

DROP TABLE IF EXISTS `vote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vote` (
  `vote_id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `message_id` int NOT NULL,
  `vote` int DEFAULT NULL,
  `vote_date` datetime DEFAULT NULL,
  PRIMARY KEY (`vote_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=gbk;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-12 23:00:22

LOCK TABLES `users` WRITE;
INSERT INTO `users` VALUES ('1', 'Administrator', 'administrator', 'Admin', null, 'administrator@happyoj.com', 'Home', 'administrator.', 'N', '1989-10-01 00:00:00', '2012-02-20 17:14:14', '223.104.4.47', '0', 'Y', '0', '0', '0', '2023-10-10 11:54:22', null, '2023-10-10 11:55:59', 'China', '11');
UNLOCK TABLES;

LOCK TABLES `privilege` WRITE;
INSERT INTO `privilege` VALUES ('1', 'Administrator', 'HEAD');
UNLOCK TABLES;

LOCK TABLES `problem` WRITE;
INSERT INTO `problem` VALUES ('1000', 'A+B Problem', 'Calculate A + B, and give me the answer!', 'Input two integers A and B.(Watch the Sample Input)', 'For each case, output A + B in one line..(Watch the Sample Output)', '1 2', '3', '0', null, ' ', '1000', '65535', 'N', '0', '0', '0', '0', 'ACSolo', null, '1', '0', '', '0', 'GUET', '0', '0', null);
UNLOCK TABLES;
