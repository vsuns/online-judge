## Usage
### Building
With CMake, judger-kernel supports a full blown build system. This way you get the most features. CMake with an equal or higher version than 3.14 is supported. With CMake it is recommended to do an out of tree build, meaning the compiled files are put in a directory separate from the source files. So in order to build judger-kernel with CMake on a Unix platform, make a build directory and run CMake inside it.

ubuntu 64 OS:  

```  
mkdir build  
cd build  
cmake -D ubuntu=1 ..  
cmake --build .   
```
binary output file is 'release/judger'

winodws 64 OS:  
```  
mkdir build  
cd build  
cmake ..  
cmake --build .   
```
binary output file is 'release/judger.exe'

### FQA
vsbuild need : https://support.microsoft.com/en-us/help/2977003/the-latest-supported-visual-c-downloads