#ifndef _CRYPT_H_
#define _CRYPT_H_

bool AES_IsCipher(unsigned char *plaintext);
int AES_Crypt_GCM_256(unsigned char *plaintext, int plaintext_len, unsigned char *ciphertext, int cipher_max_len);
int AES_Decrypt_GCM_256(unsigned char *ciphertext, int ciphertext_len, unsigned char *plaintext);

#endif

