#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h> 

#include "aes256.h"
#include "md5.h"
#include "sha256.h"
#include "base64.h"

int Encrypt_sha256(char *plaintext, char *ciphertext)
{
    sha256_context ctx;
    uint8_t hv[32];	
	//char output[64] = {0};
	
	/* do sha256 encrypt */
    sha256_init(&ctx);
    sha256_hash(&ctx, (uint8_t *)plaintext, (uint32_t)strlen(plaintext));
    sha256_done(&ctx, hv);

	/* do base64 encode */
	//base64_encode((const unsigned char *)hv, sizeof(hv) - 1, ciphertext);
	
	//uint32_t i;
	//for (i = 0; i < 32; i++) printf("%02x%s", hv[i], ((i%4)==3)?" ":"");

	//printf("\r\n output=%s, len=%d", output, strlen(output));
	//unsigned char decode[64] = {0};
	//base64_decode(output, sizeof(output) - 1, decode);
	//printf("\r\n decode=%s\r\n", decode);	

	//for (i = 0; i < 32; i++) printf("%02x%s", decode[i], ((i%4)==3)?" ":"");

	return 0;
}

#include<openssl/aes.h>
#include<openssl/rsa.h>
#include<openssl/pem.h>
#include<openssl/err.h>
#include <openssl/bn.h>

void bytes2HexStr(unsigned char *src, int srcLen, char *des)
{
	char *res;
	int i=0;
 
	res = des;
	while(srcLen > 0)
	{
		sprintf((char*)(res+i*2),"%02X",*(src+i));
		i++;
		srcLen--;
	}
}

int hex2byte(char *src, unsigned char *dst) {
    while(*src) {
        if(' ' == *src) {
            src++;
            continue;
        }
        sscanf(src, "%02X", dst);
        src += 2;
        dst++;
    }
    return 0;
}

void print_show(char *name, unsigned char *src, int len)
{
	char res[1024] = {0};
	bytes2HexStr(src, len, res);
	printf("%s: src=%s, HexStr:%s\n", name, (char *)src, res);
}

void rand_iv(unsigned char *iv, int len)
{
	int i = 0;

	BIGNUM *bn;
	bn = BN_new();
	int bits = 8 * len;
	int top = -1;
	int bottom = 1;

	BN_rand(bn, bits, top, bottom);

	char *a = BN_bn2hex(bn);
	hex2byte(a, iv);

	BN_free(bn);
}

#if 1
int openssl_aes_test()
{
	unsigned char key[16] = "1234567890";
	unsigned char iv[16] = {0};
	unsigned char iv_copy[16];

	rand_iv(iv, sizeof(iv));
	print_show("IV", iv, sizeof(iv));

	unsigned char iv_encrypt_base64[640] = {0};
	unsigned char iv_decrypt_base64[640] = {0};
	int len_iv_iv_decrypt_base64 = 640;

	base64_encode(iv, sizeof(iv), iv_encrypt_base64);
	len_iv_iv_decrypt_base64 = base64_decode(iv_encrypt_base64, iv_decrypt_base64);
	//base64_encode((const unsigned char *)iv, sizeof(iv), iv_encrypt_base64);
	//base64_decode(iv_encrypt_base64, sizeof(iv_encrypt_base64), iv_decrypt_base64);
	printf("iv_encrypt_base64:%s, %d, %d\n", iv_encrypt_base64, strlen((char*)iv_encrypt_base64));
	printf("iv_decrypt_base64:%s, %d\n",  iv_decrypt_base64, len_iv_iv_decrypt_base64);
	print_show("IV", iv_decrypt_base64, len_iv_iv_decrypt_base64);

	/* base64: 123456,          MTIzNDU2AAAAAAAAAAAA 
		       123456789012345, MTIzNDU2Nzg5MDEyMzQ1

YkDv+Ogf5q9fhtKrt+RS1NppmEt01g4oWm4k0D1GlejFwG9io8DMPDK3B3BcgH7jevD3p7cvAlSJT2OVgxtE
	*/

	char buf_normal[32] = "123456789012345678901234567890";
	unsigned char buf_encrypt[256] = "";
	unsigned char buf_encrypt_base64[256] = "";
	unsigned char buf_decrypt_base64[256] = "";
	int len_buf_decrypt_base64 = 32;
	AES_KEY aesKey;

	//加密，密文长度(明文长度 +1) / 16 * 16
	int len = ceil((strlen(buf_normal) + 1.0)/16) * 16;
	memcpy(iv_copy, iv, 16);
	AES_set_encrypt_key(key, 128, &aesKey);
	AES_cbc_encrypt((unsigned char *)buf_normal, buf_encrypt, sizeof(buf_normal), &aesKey, iv_copy, 1);
	
	printf("plaintext:%s\n", buf_normal);
	printf("ciphertext:%s, %d, %d\n", buf_encrypt, strlen((char *)buf_encrypt), len);
	print_show("buf_encrypt", buf_encrypt, len);
	base64_encode((unsigned char *)buf_encrypt, len, buf_encrypt_base64);
	printf("buf_encrypt_base64:%s, %d\n", buf_encrypt_base64, strlen((char*)buf_encrypt_base64));

	//解密
	len_buf_decrypt_base64 = base64_decode(buf_encrypt_base64, buf_decrypt_base64);
	printf("buf_decrypt_base64:%s, len=%d\n", buf_decrypt_base64, len_buf_decrypt_base64);
	print_show("buf_decrypt_base64", buf_decrypt_base64, len_buf_decrypt_base64);
	memcpy(iv_copy, iv, 16);
	AES_set_decrypt_key(key, 128, &aesKey);

	memset(buf_normal, 0, sizeof(buf_normal));
	AES_cbc_encrypt(buf_decrypt_base64, (unsigned char *)buf_normal, sizeof(buf_decrypt_base64), &aesKey, iv_copy, 0);
	printf("decrypt::%s\n", buf_normal);

	return 0;
}
#endif

int util_crypt_aes128(char *plaintext, char *ciphertext)
{
	AES_KEY aesKey;
	int bits = 16 * 8;
	unsigned char key[16] = "1234567890";
	unsigned char iv[16] = {0};
	unsigned char iv_encrypt_base64[256] = {0};
	unsigned char iv_decrypt_base64[256] = {0};

	rand_iv(iv, sizeof(iv));
	//print_show("iv", iv, 16);
	base64_encode(iv, sizeof(iv), iv_encrypt_base64); /* 编码后长度⌈n/3⌉*4, ⌈⌉ 代表上取整 */
	printf("iv_encrypt_base64 =%s\n", iv_encrypt_base64);

	unsigned char buf_encrypt[256] = {0};
	unsigned char buf_encrypt_base64[256] = {0};
	int len = 0;
	if (strlen(plaintext) <= 16) {
		len = 16;
	} else if (strlen(plaintext) <= 32) {
		len = 32;
	} else {
		return 0;
	}
	printf("util_crypt_aes128 plaintext: %s\r\n", plaintext);
	print_show("util_crypt_aes128 plaintext", (unsigned char *)plaintext, strlen(plaintext));

	AES_set_encrypt_key(key, bits, &aesKey);
	AES_cbc_encrypt((unsigned char *)plaintext, buf_encrypt, strlen(plaintext), &aesKey, iv, 1);

	printf("util_crypt_aes128 len=%d\n", len);
	print_show("util_crypt_aes128 buf_encrypt", buf_encrypt, len);
	base64_encode(buf_encrypt, len, buf_encrypt_base64);
	printf("buf_encrypt_base64 =%s\n", buf_encrypt_base64);

	//char ciphertext_temp[256] = {0};
	sprintf(ciphertext, "%s%s", iv_encrypt_base64, buf_encrypt_base64);
	//base64_encode((unsigned char*)ciphertext_temp, strlen(ciphertext_temp), (unsigned char*)ciphertext);

	return strlen(ciphertext);
}


int util_decrypt_aes128(char *ciphertext, char *plaintext)
{
	AES_KEY aesKey;
	int bits = 16 * 8;
	unsigned char key[16] = "1234567890";
	unsigned char iv[16] = {0};
	unsigned char iv_encrypt_base64[256] = {0};

	//unsigned char ciphertext_temp[256] = {0};	
	//base64_decode((unsigned char*)ciphertext, ciphertext_temp);

	int i = 0;
	for (i = 0; i < 24; i++) {
		iv_encrypt_base64[i] = ciphertext[i];
	}
	printf("iv_encrypt_base64 : %s\r\n", iv_encrypt_base64);

	base64_decode(iv_encrypt_base64, iv);

	unsigned char buf_encrypt[256] = {0};
	unsigned char buf_encrypt_base64[256] = {0};
	int j = 0;
	for (i = 24; i < strlen((char*)ciphertext); i++) {
		buf_encrypt_base64[j++] = ciphertext[i];
	}
	printf("buf_encrypt_base64 : %s\r\n", buf_encrypt_base64);

	int len = base64_decode(buf_encrypt_base64, buf_encrypt);
	printf("util_decrypt_aes128 len: %u\r\n", len);
	print_show("util_decrypt_aes128 buf_encrypt", buf_encrypt, len);
	AES_set_decrypt_key(key, bits, &aesKey);
	AES_cbc_encrypt((unsigned char *)buf_encrypt, (unsigned char *)plaintext, len, &aesKey, iv, 0);
	print_show("util_decrypt_aes128 plaintext", (unsigned char *)plaintext, strlen(plaintext));

	return strlen(plaintext);
}

int crypt_test()
{
#if 0
	extern int aes_test();
	aes_test();

	extern int md5_test();
	md5_test();

	extern int sha256_test();
	sha256_test();

	extern int base64_test();
	base64_test();

	char ciphertext[64] = {0};
	Encrypt_sha256("123456789", ciphertext);	
	printf("\r\nciphertext=%s, len=%u", ciphertext, strlen(ciphertext));

	memset(ciphertext, 0, sizeof(ciphertext));
	Encrypt_sha256("012345678901231111111111111", ciphertext);
	printf("\r\nciphertext=%s, len=%u", ciphertext, strlen(ciphertext));
	
	openssl_aes_test();
#endif

	//openssl_aes_test();

	char ans[128] = {0};
	char text[128] = {0};
	int len = 0;

	len = util_crypt_aes128("Root", ans);
	util_decrypt_aes128(ans, text);
	printf("### len=%d, cipher=%s, text=%s\n", len, ans, text);
    memset(ans, 0, sizeof(ans));
	memset(text, 0, sizeof(text));
#if 0
	len = util_crypt_aes128("Root@123", ans);
	util_decrypt_aes128(ans, text);
	printf("%d, cipher=%s, text=%s\n", len, ans, text);
    memset(ans, 0, sizeof(ans));
	memset(text, 0, sizeof(text));

	len = util_crypt_aes128("123456789012345", ans);
	util_decrypt_aes128(ans, text);
	printf("%d, cipher=%s, text=%s\n", len, ans, text);
    memset(ans, 0, sizeof(ans));
	memset(text, 0, sizeof(text));

	len = util_crypt_aes128("1234567890123456", ans);
	util_decrypt_aes128(ans, text);
	printf("%d, cipher=%s, text=%s\n", len, ans, text);
    memset(ans, 0, sizeof(ans));
	memset(text, 0, sizeof(text));

	len = util_crypt_aes128("12345678901234567", ans);
	util_decrypt_aes128(ans, text);
	printf("%d, cipher=%s, text=%s\n", len, ans, text);
    memset(ans, 0, sizeof(ans));
	memset(text, 0, sizeof(text));

	len = util_crypt_aes128("123456789012345678901234", ans);
	util_decrypt_aes128(ans, text);
	printf("%d, cipher=%s, text=%s\n", len, ans, text);
    memset(ans, 0, sizeof(ans));
	memset(text, 0, sizeof(text));

	len = util_crypt_aes128("123456789012345678901234567890", ans);
	util_decrypt_aes128(ans, text);
	printf("%d, cipher=%s, text=%s\n", len, ans, text);
    memset(ans, 0, sizeof(ans));
	memset(text, 0, sizeof(text));

	len = util_crypt_aes128("123456789012345678901234567890a", ans);
	util_decrypt_aes128(ans, text);
	printf("%d, cipher=%s, text=%s, len=%d\n", len, ans, text, strlen(text));
    memset(ans, 0, sizeof(ans));
	memset(text, 0, sizeof(text));

	len = util_crypt_aes128("123456789012345678901234567890ab", ans);
	util_decrypt_aes128(ans, text);
	printf("%d, cipher=%s, text=%s\n", len, ans, text);
    memset(ans, 0, sizeof(ans));
	memset(text, 0, sizeof(text));
#endif
	return 0;
}
#include "openssl/rand.h"
#include "openssl/evp.h"
#include "openssl/aes.h"
#include "openssl/err.h"
#define EVP_PADDING_PKCS7       1
#define MODE EVP_aes_256_gcm
const EVP_CIPHER* (*mode)() = MODE;

static void handleErrors()
{
    ERR_print_errors_fp(stderr);
}
int AesEncrypt(unsigned char *plaintext, int plaintext_len, unsigned char *key,
             unsigned char *iv, unsigned char *ciphertext)
{
    if (plaintext == NULL)
        return -1;
        
    EVP_CIPHER_CTX *ctx;
    int len;
    int ciphertext_len;

    if (!(ctx = EVP_CIPHER_CTX_new()))
        handleErrors();

    if (1 != EVP_EncryptInit_ex(ctx, mode(), NULL, key, iv))
        handleErrors();

    if (1 != EVP_CIPHER_CTX_set_padding(ctx, EVP_PADDING_PKCS7))
        handleErrors();

    if (1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len))
        handleErrors();
    ciphertext_len = len;

    if (1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len))
        handleErrors();
    ciphertext_len += len;

    EVP_CIPHER_CTX_free(ctx);

    return ciphertext_len;
}


int AesDeccrypt(unsigned char *ciphertext, int ciphertext_len, unsigned char *key,
  unsigned char *iv, unsigned char *plaintext) {
  if (ciphertext == NULL)
      return -1;

  EVP_CIPHER_CTX *ctx;

  int len;
  int plaintext_len;

  if(!(ctx = EVP_CIPHER_CTX_new())) handleErrors();

  if(1 != EVP_DecryptInit_ex(ctx, mode(), NULL, key, iv))
    handleErrors();

  if (1 != EVP_CIPHER_CTX_set_padding(ctx, EVP_PADDING_PKCS7))
    handleErrors();

  if(1 != EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len))
    handleErrors();
  plaintext_len = len;

  if(1 != EVP_DecryptFinal_ex(ctx, plaintext + len, &len)) handleErrors();
  plaintext_len += len;

  EVP_CIPHER_CTX_free(ctx);

  return plaintext_len;
}

bool AES_IsCipher(unsigned char *plaintext)
{
	int len = strlen((char *)plaintext);
	if (len < 4) {
		return 0;
	}

	if (plaintext[0] == '$' && plaintext[1] == '$' && plaintext[len-1] == '$' && plaintext[len-2] == '$') {
		return 1;
	}

	return 0;
}
int AES_Crypt_GCM_256(unsigned char *plaintext, int plaintext_len, unsigned char *ciphertext, int cipher_max_len)
{
    unsigned char *key = (unsigned char *)"";
	//printf("plaintext len %u :%s\n", plaintext_len, plaintext);

	unsigned char saved_iv[12];
	RAND_pseudo_bytes(saved_iv, sizeof(saved_iv));
	//BIO_dump_fp(stdout, (const char *)saved_iv, sizeof(saved_iv));
	unsigned char buf_vi_base64[32] = {0};
	//BIO_dump_fp(stdout, (const char *)saved_iv, sizeof(saved_iv));

	int len_iv = base64_encode(saved_iv, sizeof(saved_iv), buf_vi_base64);
	//printf("iv base64 len %u :%s\n", len_iv, buf_vi_base64);
	
    unsigned char cipher[256] = {0};
	int ciphertext_len = AesEncrypt(plaintext, strlen((char *)plaintext), key, saved_iv, cipher);
    //printf("Ciphertext len %u :\n", ciphertext_len);
    //BIO_dump_fp(stdout, (const char *)cipher, ciphertext_len);

	unsigned char buf_cipher_base64[512] = {0};
	int len = base64_encode(cipher, ciphertext_len, buf_cipher_base64);
	//printf("Ciphertext base64 len %u :%s\n", len, buf_cipher_base64);
	//printf("Result: %s%s\n", buf_vi_base64, buf_cipher_base64);

	sprintf((char *)ciphertext, "$$%s%s$$", buf_vi_base64, buf_cipher_base64);

	if (strlen((char *)ciphertext) != len_iv + len + 4) {
		printf("AES_Crypt_GCM_256 failed. len not the same. %u != %u :\n", strlen((char *)ciphertext), len_iv + len + 4);
	}

	return strlen((char *)ciphertext);
}

int AES_Decrypt_GCM_256(unsigned char *ciphertext, int ciphertext_len, unsigned char *plaintext) {
	unsigned char iv[12] = {0};
	unsigned char iv_base64[17] = {0};
	memcpy(iv_base64, ciphertext + 2 , 16);
	//printf("iv base64 len %u :%s\n", strlen((char *)iv_base64), iv_base64);
	int iv_len = base64_decode(iv_base64, iv);
	//printf("iv_len %u :\n", iv_len);
	//BIO_dump_fp(stdout, (const char *)iv, iv_len);
	
	unsigned char real_cipher_base64[128] = {0};
	memcpy(real_cipher_base64, ciphertext + 2 + 16, ciphertext_len - 2 - 16 - 2);
	//printf("real_cipher_base64 len %u :%s\n", strlen((char *)real_cipher_base64), real_cipher_base64);

	unsigned char real_cipher[128] = {0};
	int cipher_len = base64_decode(real_cipher_base64, real_cipher);
	//printf("Ciphertext len %u :\n", cipher_len);
	//BIO_dump_fp(stdout, (const char *)real_cipher, cipher_len);

	unsigned char *key = (unsigned char *)"";
	int decryptedtext_len = AesDeccrypt(real_cipher, cipher_len, key, iv, plaintext);
    plaintext[decryptedtext_len] = '\0';
	//printf("plaintext len %u : %s\n", decryptedtext_len, plaintext);
	return decryptedtext_len;
}

int crypt_test2()
{
	unsigned char *plaintext = (unsigned char *)"weizengke@123";
	unsigned char ciphertext[128] = {0};
	int len = AES_Crypt_GCM_256(plaintext, strlen((char *)plaintext), ciphertext, sizeof(ciphertext));
	printf("Ciphertext len %u :\n", len);
	printf("%s\n", ciphertext);

	unsigned char decryptedtext[128] = {0};
	AES_Decrypt_GCM_256(ciphertext, len, decryptedtext);
	printf("Plaintext len %u :\n", len);
	printf("%s\n", decryptedtext);

}

int crypt_test1()
{
    unsigned char *key = (unsigned char *)"";
    unsigned char *plaintext = (unsigned char *)"weizengke@123";
    unsigned char ciphertext[128] = {0};
    unsigned char decryptedtext[128] = {0};
    int decryptedtext_len, ciphertext_len;

	unsigned char saved_iv[12];
	RAND_pseudo_bytes(saved_iv, sizeof(saved_iv));
	BIO_dump_fp(stdout, (const char *)saved_iv, sizeof(saved_iv));
	unsigned char buf_vi_base64[1000] = {0};
	int len_iv = base64_encode(saved_iv, sizeof(saved_iv), buf_vi_base64);
	printf("iv base64 len %u :\n", len_iv);
	printf("%s\n", buf_vi_base64);

    ciphertext_len = AesEncrypt(plaintext, strlen((char *)plaintext), key, saved_iv, ciphertext);

    printf("Ciphertext len %u :\n", ciphertext_len);
    BIO_dump_fp(stdout, (const char *)ciphertext, ciphertext_len);

	unsigned char buf_encrypt_base64[1000] = {0};
	int len = base64_encode(ciphertext, ciphertext_len, buf_encrypt_base64);
	printf("Ciphertext base64 len %u :\n", len);
	printf("%s%s\n", buf_vi_base64, buf_encrypt_base64);

    decryptedtext_len = AesDeccrypt(ciphertext, ciphertext_len, key, saved_iv, decryptedtext);
    decryptedtext[decryptedtext_len] = '\0';

    printf("Decrypted text len %u:\n", decryptedtext_len);
    printf("%s\n", decryptedtext);
    return 0;
}