#include "judge/include/judge_inc.h"

#ifdef _LINUX_
#include <csignal>
#include <wait.h>
#include <sys/resource.h>
#include <pwd.h>
#include <dirent.h>
#include <sys/user.h>
#include <sys/ptrace.h>

#if (OS_YES == OSP_MODULE_JUDGE)
using namespace std;

int judge_get_testcases(JUDGE_SUBMISSION_S *submission, char szTestcases[JUDGE_MAX_CASE][UTIL_MAX_FNAME])
{   
    char szPath[UTIL_MAX_PATH] = {0};
    int caseNum = 0;
	DIR *dir;
	struct dirent *ptr;

	if (submission->mode == JUDGE_TEST_MODE && submission->solution.problemId == 0) {
		sprintf(szTestcases[caseNum++], "data");
		return caseNum;
	}

    sprintf(szPath, "%s\/%d", g_judge_testcase_path, submission->problem.problemId);

	if ((dir = opendir(szPath)) == NULL) {
		return caseNum;
	}
 
	while ((ptr = readdir(dir)) != NULL) {
		if(ptr->d_type != 8) { /* file */
            continue;
		}

		char drive[UTIL_MAX_DRIVE] = {0};
		char dir[UTIL_MAX_DIR] = {0};
		char fname[UTIL_MAX_FNAME] = {0};
		char ext[UTIL_MAX_EXT] = {0};

		util_splitpath(ptr->d_name, drive, dir, fname, ext);
        if (0 != strcmp(ext, ".in")) {
            continue;
        }

		sprintf(szTestcases[caseNum++], "%s", fname);
	}

	closedir(dir);

    qsort(szTestcases, caseNum, sizeof(char)*UTIL_MAX_FNAME, string_cmp);

    return caseNum;
}

void judge_solution_memory_check(JUDGE_SUBMISSION_S *submission)
{    
    if (submission->solution.memory_cur < submission->rused.ru_maxrss) {
        submission->solution.memory_cur = submission->rused.ru_maxrss;
        if (submission->solution.memory_cur > submission->solution.memory_used) {
            submission->solution.memory_used = submission->solution.memory_cur;
        }                
    } 
    
    if (submission->solution.memory_cur > submission->problem.memory_limit) {
        submission->solution.verdictId = V_MLE;
        submission->solution.memory_used = submission->problem.memory_limit;
    }
}

void judge_solution_exit_check(JUDGE_SUBMISSION_S *submission)
{
    int status = submission->status;
    write_log(JUDGE_INFO, "solution exit: status=%d, WIFEXITED:%d, "
                "WEXITSTATUS:%d, WIFSIGNALED:%d, WIFSTOPPED:%d, WSTOPSIG:%d SIGTRAP=%d.", 
                status, WIFEXITED(status), WEXITSTATUS(status), WIFSIGNALED(status),
                WIFSTOPPED(status), WSTOPSIG(status), SIGTRAP);

    /* check exit normally */
    if (WIFEXITED(status)) {
        if (WEXITSTATUS(status) == 0) {
            write_log(JUDGE_INFO, "solution run finish.");
        } else {
            if (WEXITSTATUS(status) == 11) {
                return;
            }
            //submission->solution.verdictId = V_SE;
            write_log(JUDGE_ERROR, "abnormal quit, exit_code: %d", WEXITSTATUS(status));
        }
        return;
    }

    // RE/TLE/OLE
    if (WIFSIGNALED(status) || (WIFSTOPPED(status) && WSTOPSIG(status) != SIGTRAP)) {
        int signo = 0;
        if (WIFSIGNALED(status)) {
            signo = WTERMSIG(status);
            write_log(JUDGE_ERROR, "child process killed by signal %d, %s", signo, strsignal(signo));
        } else {
            signo = WSTOPSIG(status);
            write_log(JUDGE_INFO, "child process stopped by signal %d, %s", signo, strsignal(signo));
        }

        switch (signo) {
            // Ignore
            case SIGCHLD:
                write_log(JUDGE_ERROR, "SIGCHLD");
                break;
                // TLE
            case SIGALRM:    // alarm() and setitimer(ITIMER_REAL)
            case SIGVTALRM:  // setitimer(ITIMER_VIRTUAL)
            case SIGXCPU:    // exceeds soft processor limit
                write_log(JUDGE_ERROR, "Time Limit Exceeded: %s", strsignal(signo));
                submission->solution.verdictId = V_OLE;
                break;
                // OLE
            case SIGXFSZ:  // exceeds file size limit
                write_log(JUDGE_ERROR, "Output Limit Exceeded: %s", strsignal(signo));
                submission->solution.verdictId = V_OLE;
                break;
                // RE
            case SIGSEGV:  // segmentation violation
            case SIGFPE:   // any arithmetic exception
            case SIGBUS:   // the process incurs a hardware fault
            case SIGABRT:  // abort() function
            case SIGKILL:  // exceeds hard processor limit
                write_log(JUDGE_ERROR, "Runtime Error: %s", strsignal(signo));
                submission->solution.verdictId = V_RE;
                break;
            default:
                submission->solution.verdictId = V_SE;
                write_log(JUDGE_ERROR,"signo default: %s", strsignal(signo));
                break;
        } 

        kill(submission->exec_pid, SIGKILL);
        return;
    } 

    return;
}

void judge_solution_time_check(JUDGE_SUBMISSION_S *submission)
{
    long sec = submission->rused.ru_utime.tv_sec + submission->rused.ru_stime.tv_sec;
    long usec = submission->rused.ru_utime.tv_usec + submission->rused.ru_stime.tv_usec;
    usec += sec * 1000000;
    usec /= 1000;
    submission->solution.time_cur = usec;

    if (submission->solution.time_cur < 0) {
        submission->solution.time_cur = submission->problem.time_limit;
    }

    /* more than time_used */
    if (submission->solution.time_cur > submission->solution.time_used) {
        submission->solution.time_used = submission->solution.time_cur;
    }
    
    /* more than time_limit */
    if (submission->solution.time_used >= submission->problem.time_limit) {
        submission->solution.verdictId = V_TLE;
        submission->solution.time_used = submission->problem.time_limit;
    }
}

int judge_set_limit(JUDGE_SUBMISSION_S *submission) 
{
    rlimit rl = {0};

    rl.rlim_cur = submission->problem.time_limit / 1000;
    rl.rlim_max = rl.rlim_cur + 1;
    setrlimit(RLIMIT_CPU, &rl);
 
    rl.rlim_cur = submission->problem.memory_limit * 1024;
    rl.rlim_max = rl.rlim_cur + 1024;
    setrlimit(RLIMIT_DATA, &rl);
    return OS_OK;
}

int judge_chroot(JUDGE_SUBMISSION_S *submission) {

    char cwd[256];
    char *tmp = getcwd(cwd, 256 - 1);
    if (tmp == nullptr) {
        return OS_ERR;
    }

    if (0 != chdir(submission->subPath)) {
        return OS_ERR;
    }
    
    return OS_OK;
}

void judge_split(char **arr, char *str, const char *del){
    char *s = NULL;
    s = strtok(str, del);
    while(s != NULL) {
        write_log(JUDGE_INFO, "%s", s);
        *arr++ = s;
        s = strtok(NULL, del);
    }
    *arr++ = NULL;
}

int judge_child_process_run(JUDGE_SUBMISSION_S *submission)
{
    if (OS_OK != judge_set_limit(submission)) {
        exit(3);
    }
#if 0
    if (ptrace(PTRACE_TRACEME, 0, NULL, NULL) < 0) {
        write_log(JUDGE_ERROR, "PTRACE_TRACEME failed: %s", strerror(errno));
        exit(4);
    }
#endif

    int newstdin = open(submission->inFileName, O_RDWR|O_CREAT, 0644);
    int newstdout = open(submission->outFileName, O_RDWR|O_CREAT, 0644);
    if (newstdout != -1 && newstdin != -1){
        dup2(newstdout, fileno(stdout));
        dup2(newstdin, fileno(stdin));
        char *args[20];
        judge_split(args, submission->runCmd, " ");
        int ret = execvp(args[0], args);
        close(newstdin);
        close(newstdout);
        if (ret == -1) {
            write_log(JUDGE_ERROR, "Failed to execvp solution. (ret=%d, runCmd=%s)", ret, submission->runCmd);
            exit(12);
            return OS_ERR;
        }
        exit(11);
    } else {
        write_log(JUDGE_ERROR, "Failed to open inFile(%s) or outFile(%s).",
            submission->inFileName, submission->outFileName);
    }
    return OS_OK;
}

void judge_child_process_monitor(JUDGE_SUBMISSION_S *submission)
{
    int status = 0;
    do {
        if (wait4(submission->exec_pid, &status, 0, &submission->rused) < 0) {
            write_log(JUDGE_ERROR, "wait4 executor failed: %s", strerror(errno));
            kill(submission->exec_pid, SIGKILL);
            exit(0);
        }
        submission->status = status;
    }while(0);
}

int judge_solution_run(JUDGE_SUBMISSION_S *submission)
{
    pid_t exec_pid = fork();
    if (exec_pid < 0) {
        write_log(JUDGE_ERROR,"solution fork failed. [%s]", strerror(errno));
    } else if (exec_pid == 0) {
        if (judge_child_process_run(submission) != OS_OK) {
            return OS_ERR;
        }
    } else {
        submission->exec_pid = exec_pid;
        judge_child_process_monitor(submission);
    }

    return OS_OK;
}

int judge_compile_run(JUDGE_SUBMISSION_S *submission)
{
    FILE *f = fopen(submission->DebugFile, "w");
	fprintf(f, "");
    fclose(f);

    pid_t pid = fork();
    if (pid < 0) {
        write_log(JUDGE_ERROR,"compile fork err.");
    } else if (pid == 0) {
        /* set compile time limit */
        rlimit limT{};
        limT.rlim_cur = limT.rlim_max = 10;
        if (setrlimit(RLIMIT_CPU, &limT) < 0) {
            write_log(JUDGE_ERROR,"setrlimit RLIMIT_CPU failed: %s", strerror(errno));
            exit(3);         
        }

        /* 限制100M */
        rlimit limM{};
        limM.rlim_cur = 1024 * 1024 * submission->compile_memory_max_mb;
        limM.rlim_max = limM.rlim_cur;
        if (setrlimit(RLIMIT_DATA, &limM) < 0) {
            write_log(JUDGE_ERROR,"setrlimit RLIMIT_MEM failed: %s", strerror(errno));
            exit(4);          
        }

        int newstdout = open(submission->DebugFile, O_RDWR|O_CREAT, 0644);
        if (newstdout == -1) {
            write_log(JUDGE_ERROR, "Failed to open DebugFile(%s).", submission->DebugFile);
           exit(5);
        }

        dup2(newstdout, fileno(stderr));
        char *args[20];
        judge_split(args, submission->compileCmd, " ");
        int ret = execvp(args[0], args);
        close(newstdout);

        if (ret == -1) {
            write_log(JUDGE_ERROR, "Failed to execvp compile. (ret=%d, runCmd=%s)", ret, submission->compileCmd);
            exit(6);
        }
        
        write_log(JUDGE_INFO,"compile thread end. (solution=%u)", submission->solution.solutionId);
        exit(0);
    } else {
        rusage rused;
        int status = 0;
        if (wait4(pid, &status, 0, &rused) < 0) {
            write_log(JUDGE_ERROR, "solution %u compile timeout: %s", submission->solution.solutionId, strerror(errno));
            kill(pid, SIGKILL);
            exit(0);
        }

        write_log(JUDGE_INFO, "solution %u compile process exit status %d, WIFEXITED=%d, WEXITSTATUS=%d, memory used(%d)/max(%d) MB",
            submission->solution.solutionId, status, WIFEXITED(status), WEXITSTATUS(status), rused.ru_maxrss/1024, submission->compile_memory_max_mb);

        if (WIFEXITED(status)) {
            write_log(JUDGE_INFO, "solution %u compile finish", submission->solution.solutionId);
        } else {
            if (WIFSIGNALED(status)) {
                write_log(JUDGE_ERROR, "compile kill by %s", strsignal(WTERMSIG(status)));
            }
        }
    }

	return OS_OK;
}
#endif

#endif
