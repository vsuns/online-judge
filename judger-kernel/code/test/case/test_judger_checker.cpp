#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "judge/include/judge_def.h"
#include "judge/include/judge_util.h"

#define MY_ASSERT_EQ(expect,input)\
	if (expect==input){ASSERT_TRUE(expect==input);} else {printf("expect %d, but input %d\r\n",expect,input);ASSERT_TRUE(expect==input);}

TEST(judger_checker_suite, case1) {
	int ret = compare("test/data_1.in","test/data_ans.in");
	MY_ASSERT_EQ(V_AC,ret);
}

TEST(judger_checker_suite, case2) {
	int ret = compare("test/data_2.in","test/data_ans.in");
	MY_ASSERT_EQ(V_AC,ret);
}

TEST(judger_checker_suite, case3) {
	int ret = compare("test/data_3.in","test/data_ans.in");
	MY_ASSERT_EQ(V_WA,ret);
}

TEST(judger_checker_suite, case4) {
	int ret = compare("test/data_4.in","test/data_ans.in");
	MY_ASSERT_EQ(V_PE,ret);
}

TEST(judger_checker_suite, case5) {
	int ret = compare("test/data_5.in","test/data_ans.in");
	MY_ASSERT_EQ(V_WA,ret);
}

TEST(judger_checker_suite, case6) {
	int ret = compare("test/data_6.in","test/data_ans.in");
	MY_ASSERT_EQ(V_PE,ret);
}

TEST(judger_checker_suite, mul_line_no_cr_return_wa) {
	int ret = compare("test/1_1.in","test/1_ans.in");
	MY_ASSERT_EQ(V_WA,ret);
}
TEST(judger_checker_suite, ignore_space_of_line_tailer) {
	int ret = compare("test/1_2.in","test/1_ans.in");
	MY_ASSERT_EQ(V_AC,ret);
}

TEST(judger_checker_suite, NULL_equal_CR) {
	int ret = compare("test/2_1.out","test/2_ans.out");
	MY_ASSERT_EQ(V_AC,ret);
}

using namespace testing;

typedef struct c_struct_t{
    char *p_c_pointer;
    int(*p_c_func)(struct c_struct_t*);
}c_struct_t;

int c_func_caller(c_struct_t* p_c_struct) {
	return 456;
}

/*构造mock*/
//定义mock类
class Mock_FOO {
	public:
		//定义mock方法
		MOCK_METHOD1(set, int(int num));
		//定义mock方法
		MOCK_METHOD0(get, int());
		//定义mock方法
		MOCK_METHOD1(mock_c_func, int(c_struct_t *p_c_struct));
};
//实例化mock对象
Mock_FOO mocker;
//创建mock对象方法的函数的C包装
int mock_c_func(c_struct_t *p_c_struct) {
    return mocker.mock_c_func(p_c_struct);
}

//测试被测函数
TEST(Atest, test_c_func_000)
{
    EXPECT_CALL(mocker, mock_c_func(IsNull())).WillRepeatedly(Return(654));

    c_struct_t c_struct_foo;
    c_struct_foo.p_c_func = mock_c_func;
    int ret = c_func_caller(&c_struct_foo);
    EXPECT_EQ(456, ret);
}
